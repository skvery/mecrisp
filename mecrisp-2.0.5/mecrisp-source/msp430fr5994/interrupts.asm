;
;    Mecrisp - A native code Forth implementation for MSP430 microcontrollers
;    Copyright (C) 2011  Matthias Koch
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

; Interrupt vectors and handlers that can be exchanged on the fly.

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port1"
  CoreVariable irq_hook_port1
;------------------------------------------------------------------------------
  pushda #irq_hook_port1
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port1:
  push r7
  call &irq_hook_port1
  pop r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port2"
  CoreVariable irq_hook_port2
;------------------------------------------------------------------------------
  pushda #irq_hook_port2
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port2:
  push r7
  call &irq_hook_port2
  pop r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port3"
  CoreVariable irq_hook_port3
;------------------------------------------------------------------------------
  pushda #irq_hook_port3
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port3:
  push r7
  call &irq_hook_port3
  pop r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port4"
  CoreVariable irq_hook_port4
;------------------------------------------------------------------------------
  pushda #irq_hook_port4
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port4:
  push r7
  call &irq_hook_port4
  pop r7
  reti
  
;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port5"
  CoreVariable irq_hook_port5
;------------------------------------------------------------------------------
  pushda #irq_hook_port5
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port5:
  push r7
  call &irq_hook_port5
  pop r7
  reti
  
;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port6"
  CoreVariable irq_hook_port6
;------------------------------------------------------------------------------
  pushda #irq_hook_port6
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port6:
  push r7
  call &irq_hook_port6
  pop r7
  reti
  
;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port7"
  CoreVariable irq_hook_port7
;------------------------------------------------------------------------------
  pushda #irq_hook_port7
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port7:
  push r7
  call &irq_hook_port7
  pop r7
  reti
  
;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-port8"
  CoreVariable irq_hook_port8
;------------------------------------------------------------------------------
  pushda #irq_hook_port8
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_port8:
  push r7
  call &irq_hook_port8
  pop r7
  reti  

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-adc"
  CoreVariable irq_hook_adc
;------------------------------------------------------------------------------
  pushda #irq_hook_adc
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_adc:
  push r7
  call &irq_hook_adc
  pop r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-watchdog"
  CoreVariable irq_hook_watchdog
;------------------------------------------------------------------------------
  pushda #irq_hook_watchdog
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_watchdog:
  push r7
  call &irq_hook_watchdog
  pop r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-rtc"
  CoreVariable irq_hook_rtc
;------------------------------------------------------------------------------
  pushda #irq_hook_rtc
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_rtc:
  .word 1407h ; push.a r7
  call &irq_hook_rtc
  .word 1607h ; pop.a r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-comp"
  CoreVariable irq_hook_comp
;------------------------------------------------------------------------------
  pushda #irq_hook_comp
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_comp:
  push r7
  call &irq_hook_comp
  pop r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-tb0ccr0"
  CoreVariable irq_hook_tb0ccr0
;------------------------------------------------------------------------------
  pushda #irq_hook_tb0ccr0
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_tb0ccr0:
  push r7
  call &irq_hook_tb0ccr0
  pop r7
  reti

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma0"
  CoreVariable irq_hook_dma0
;------------------------------------------------------------------------------
  pushda #irq_hook_dma0
  ret
  .word nop_vektor  ; Initial value for unused interrupts

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma1"
  CoreVariable irq_hook_dma1
;------------------------------------------------------------------------------
  pushda #irq_hook_dma1
  ret
  .word nop_vektor  ; Initial value for unused interrupts

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma2"
  CoreVariable irq_hook_dma2
;------------------------------------------------------------------------------
  pushda #irq_hook_dma2
  ret
  .word nop_vektor  ; Initial value for unused interrupts

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma3"
  CoreVariable irq_hook_dma3
;------------------------------------------------------------------------------
  pushda #irq_hook_dma3
  ret
  .word nop_vektor  ; Initial value for unused interrupts

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma4"
  CoreVariable irq_hook_dma4
;------------------------------------------------------------------------------
  pushda #irq_hook_dma4
  ret
  .word nop_vektor  ; Initial value for unused interrupts

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma5"
  CoreVariable irq_hook_dma5
;------------------------------------------------------------------------------
  pushda #irq_hook_dma5
  ret
  .word nop_vektor  ; Initial value for unused interrupts

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma6"
  CoreVariable irq_hook_dma6
;------------------------------------------------------------------------------
  pushda #irq_hook_dma6
  ret
  .word nop_vektor  ; Initial value for unused interrupts

;------------------------------------------------------------------------------
  Wortbirne Flag_visible|Flag_Variable, "irq-dma7"
  CoreVariable irq_hook_dma7
;------------------------------------------------------------------------------
  pushda #irq_hook_dma7
  ret
  .word nop_vektor  ; Initial value for unused interrupts

irq_vektor_dma0:
  push r7
  call &irq_hook_dma0
  pop r7
  reti

irq_vektor_dma1:
  push r7
  call &irq_hook_dma1
  pop r7
  reti

irq_vektor_dma2:
  push r7
  call &irq_hook_dma2
  pop r7
  reti

irq_vektor_dma3:
  push r7
  call &irq_hook_dma3
  pop r7
  reti

irq_vektor_dma4:
  push r7
  call &irq_hook_dma4
  pop r7
  reti

irq_vektor_dma5:
  push r7
  call &irq_hook_dma5
  pop r7
  reti

irq_vektor_dma6:
  push r7
  call &irq_hook_dma6
  pop r7
  reti

irq_vektor_dma7:
  push r7
  call &irq_hook_dma7
  pop r7
  reti
  
DMAIV equ 050Eh     ; 2017-03-17

irq_vektor_dma:
  add &DMAIV, pc      ; Add offset to Jump table 3
  reti                ; Vector 0: No interrupt 5
  jmp irq_vektor_dma0 ; Vector 2: DMA channel 0 2
  jmp irq_vektor_dma1 ; Vector 4: DMA channel 1 2
  jmp irq_vektor_dma2 ; Vector 6: DMA channel 2 2
  jmp irq_vektor_dma3 ; Vector 8: DMA channel 3 2
  jmp irq_vektor_dma4 ; Vector 10: DMA channel 4 2
  jmp irq_vektor_dma5 ; Vector 12: DMA channel 5 2
  jmp irq_vektor_dma6 ; Vector 14: DMA channel 6 2
  jmp irq_vektor_dma7 ; Vector 16: DMA channel 7 2
