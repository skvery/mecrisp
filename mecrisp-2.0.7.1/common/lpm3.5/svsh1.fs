\ svsh1.fs
\
\ Steps to implement:
\
\ 1 flash launchpad with msp430fr5994.hex
\ 2 download and run folie from https://github.com/jeelabs/embello/releases
\ 3 select correct serial port in folie ( raw mode and 115 kbaud might be needed )
\ 4 reset or enter for welcome message
\ 5 1 1 + gives 2 ok
\ !send svsh1.fs
\ reset to wake up led - enter to toggle led2
\ unplug board and wait for complete power down
\ type show for diagnostics
\ 
\ svsh test 1 for msp430fr5994
\ ============================
\ Only state 1-3 is implemented:
\
\ 1. normal running on power state with led1 and led2 on. ( led2 is toggled under rx )
\ 2. lpm3.5 state - only the 32-bit epoch counter is running and led1 is toggled.
\ 3. no-power state
\
\ The following transitions are defined:
\
\ 1-2: triggered by s1
\ 2-3: interrupt triggered by SVSH is used to save counter to fram
\ 3-1: repowering the device and normal startup and counter re-load
\ 1-3: triggered by no-power shutdown, also under SVSH interrupt
\ 2-1-2: wake up interrupt triggered by s2
\
\ Notes:
\
\ - for testing led 1 is jumpered to j1.1 (3v3) to drain supecap.
\ - the launchpad is unplugged to drain cap and cause 1-3
\ - SVSH interrupt will be followed by BOR.
\  
eraseflash
compiletoflash
\ define msp430fr5994 constants used
$01 constant .0
$02 constant .1
$04 constant .2
$08 constant .3
$10 constant .4
$20 constant .5
$40 constant .6
$80 constant .7
$0100 constant .8
$0200 constant .9
$0400 constant .10
$0800 constant .11
$1000 constant .12
$2000 constant .13
$4000 constant .14
$8000 constant .15

$0200 constant P1Base       \ P1, P2 Registers (Base Address: 0200h)
$0320 constant PJBase       \ PJ Registers (Base Address: 0320h)
  $02 constant PxOUT           \ Port Px output P1OUT 02h
  $04 constant PxDIR           \ Port Px direction P1DIR 04h
  $0A constant PxSEL0          \ Port Px selection 0 P1SEL0 0Ah
$0120 constant PMMBase
  $A5 constant PWD_PMM
  .2  constant PMMSWBOR
  $00 constant PMMCTL0
  $01 constant PMMPW
  $0A constant PMMIFG
  $10 constant PM5CTL0
  .0  constant LOCKLPM5
$019E constant SYSRSTIV

$04A0 constant RTCBASE        \ RTC_C Registers (Base Address: 04A0h)
  $A5 constant PWD_RTC
  $01 constant RTCPWD         \ RTC password RTCPWD 01h
  $02 constant RTCCTL1        \ RTC control 1 RTCCTL1 02h
   .6 constant RTCHOLD                 \ 6 RTCHOLD RW 1 for hold 
    .3 constant RTCSSEL                 \ 3-2 1x SSEL RT1PS
  $08 constant RTCPS0CTL      \ RTC prescaler 0 control RTCPS0CTL 08h
 .13 .12 + .11 + constant RT0PSDIV   \ 13-11  111 = Divide by 256

  $0A constant RTCPS1CTL      \ RTC prescaler 1 control RTCPS1CTL 0Ah
    .15       constant RT1SSEL          \ 15-14 1x SSEL RT0PS
    .13 .12 + constant RT1PSDIV         \ 13-11  110 = Divide by 128
    .4 .3 +   constant RT1IP            \ 4-2 RT1IPx RW 0h 110b = Divide by 128 for 1 Hz

  $10 constant RTCCNT12         \ RTC seconds/counter 12 RTCSEC/RTCNT1 10h
  $10 constant RTCCNT1          \ RTC seconds/counter 1 RTCSEC/RTCNT1 10h
  $11 constant RTCCNT2          \ RTC minutes/counter 2 RTCMIN/RTCNT2 11h
  $12 constant RTCCNT34         \ RTC hours/counter 34 RTCHOUR/RTCNT3 12h
  $12 constant RTCCNT3          \ RTC hours/counter 3 RTCHOUR/RTCNT3 12h
  $13 constant RTCCNT4          \ RTC day of w/counter 4 RTCDOW/RTCNT4 13h
\
: toggle1 ( -- )              \ Toggles P1.0
  .0 PxOUT P1Base + cxor!
;
: toggle2 ( -- )              \ Toggles P1.1
  .1 PxOUT P1Base + cxor!
;
: on1 ( -- )
  .0 PxOUT P1Base + cbis!
;
\ read and print conters   
: 2sec. ( -- ud )           \ read 32-bit counter ( no hold checks )
  RTCCNT1 RTCBASE + 2@
  swap hex. hex.
;
\ information memory as follows
$1800 constant TRAPDOOR  \ reset function pointer
$1802 constant CNT1
$1803 constant CNT2
$1804 constant CNT3
$1805 constant CNT4
$1806 constant RSTP       \ reset pointer
: swbor ( -- na )                      \ software defined BOR with trapdoor bypass
  true TRAPDOOR !                      \ reset trapdoor first
  PWD_PMM PMMPW PMMBase + c!           \ unlock PMM
  PMMSWBOR PMMCTL0 PMMBase + cbis!     \ BOR
  0       PMMPW PMMBase + c!           \ lock PMM
;
: svshreset ( -- )
  LOCKLPM5 PM5CTL0 PMMBase + bic!   \ clear LOCKLPM5 (nop)
  RTCCNT1 RTCBASE + c@ CNT1  c!     \ 
  RTCCNT2 RTCBASE + c@ CNT2  c!     \ 
  RTCCNT3 RTCBASE + c@ CNT3  c!     \ 
  RTCCNT4 RTCBASE + c@ CNT4  c!     \ 
  true TRAPDOOR !                   \ clear trapdoor
  2 RSTP +!                         \ inc rstp
  RTCCNT1 RTCBASE + c@ 10 + $FF and \ RTCCNT1 + 10
  begin                             \ wait   
    dup RTCCNT1 RTCBASE + c@ 
    = 
  until drop                        \ wait for bor
  swbor ( -- na )                   \ terminate program flow
;
: wake-up ( -- )               \ init placeholder for trapdoor startup configuration
  PMMIFG PMMBase + @ $1808 RSTP @ + !   \ log PMMIFG 
  2 RSTP +!
  SYSRSTIV @ dup RSTP @ $1808 + !       \ log SYSRSTIV 
  case                                  \ SYSRSTIV case
    $0E of                              \ SVSH = $0E
      svshreset
    endof
  endcase
  2 RSTP +!
  true TRAPDOOR !            \ clear trapdoor
  swbor                      \ normal reset 
;
: eraseflash ( -- )  
  true TRAPDOOR !            \ cancel trapdoor 
  eraseflash                 \ chain
;
: zero ( -- )
  cr ." Reading SYSRSTIV:"
  begin
    SYSRSTIV @ dup
    cr ." = " hex.
    0=
  until
  cr
  0 RSTP !
  true CNT1 !
  true CNT3 !
;
: show ( -- )                \ read log info from information memory
  cr
  cr ." 2sec. = " 2sec.
  cr ." 0x1800 = " TRAPDOOR @ hex. ." (TRAPDOOR)" 
  cr ." 0x1802 = " CNT1 @ hex. ." (CNT12)" 
  cr ." 0x1804 = " CNT3 @ hex. ." (CNT34)" 
  cr
  cr ." 0x1806 = " RSTP @ hex. ." (RSTP)"
  RSTP @ 4 / 0 ?do
    cr ." 0x" $1808 i 4 * + dup hex. ." = " @ hex. ." (PMMIFG)"
    cr ." 0x" $180A i 4 * + dup hex. ." = " @ hex. ." (SYSRSTIV)"
  loop 
  cr
  zero
;
: serial-key toggle2 serial-key ;       \ line rx will toggle led2
: init
  .0 .1 + PxDIR  P1Base + c! \ P1, P2 clear P1.0 and P1.1 to LED output direction
  on1               \ led1 on
  \ rtc setup
  .4      PxSEL0 PJBase + c!  \ PJ SEL0 lfxt
  PWD_RTC  RTCBASE RTCPWD  + c!         \ unlock
  RTCSSEL  RTCBASE RTCCTL1 + c!         \ setup counter mode.
  RT0PSDIV RTCBASE RTCPS0CTL + !
  RT1SSEL RT1PSDIV + RT1IP + RTCBASE RTCPS1CTL + !
  RTCHOLD RTCBASE RTCCTL1 + cbic!       \ clear the RTCHOLD and bit to start the counter.
  0 RTCBASE RTCPWD + c!                 \ lock
  \ arm trapdoor
  ['] wake-up TRAPDOOR !                \ actvate reboot trapdoor
  ['] serial-key hook-key !             \ fuction pointer
  cr ." Type 'show' to typeand reset log:" cr
;
