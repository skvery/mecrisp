\ main.fs
\
\ lpm3.5 demo for fram devices
\ 
\ main file - import of all modules
\ ---------------------------------
\
\ This is a long code sample that can be used as
\ basis for a program that needs a one second epoch timer
\ that keeps running under battery or supercap power.  It
\ also demonstrates the use of interrupts to wake up.
\
\ Note that it might be considered bad style to code all
\ MSP430 constants.  This can be removed to save space
\ if space is a premium.
\
\ The <<lpm3.5>> cornerstone is used to keep the code
\ in fram.
\
\ Three states are implemented:
\
\ 1. normal running on power state with flashing led2.
\ 2. lpm3.5 state - only the 32-bit epoch counter is running and led1 is toggled.
\ 3. no-power state
\
\ The following transitions are defined:
\
\ 1-2: triggered by s1
\ 2-3: interrupt triggered by SVSH is used to save counter to fram
\ 3-1: repowering the device and normal startup and counter re-load
\ 1-3: triggered by no-power shutdown, also under SVSH interrupt
\ 2-1-2: wake up interrupt triggered by s2
\
\
\ Notes:
\
\ - for SVSH interrupt see lpm5.fs
\ - for switch and led setup see setupports.fs
\ - SVSH interrupt will be followed by BOR.
\  
\
\ Low Power Mode 3.5 Backdoor Demo with RTC
\ -----------------------------------------
\
\ For a complete description of LPMx.5 refer to the user
\ guide, slau367.
\ The low-power modes LPM3.5 and LPM4.5 (LPMx.5) give the 
\ lowest power consumption on a device.
\ In LPMx.5, the core LDO of the device is switched off.
\
\ ( see note in slau367 about wake-up time )
\
\ The active Forth program must prepare and enter LPMx.5
\ see 1.4.3.1. 
\
\ To allow for the minimum start-up clock cycles Mecrisp
\ looks for a trapdoor located at $1800 in information FRAM.
\ The code pointer at $1800 is responsible to restore the
\ state according to 1.4.3.2.
\
\ In this example:
\
\ - led2 is toggled and the epoch counter is incremented and saved
\   each second using timer interrupt while Mecrisp runs normally
\ - pressing s1 invokes LPM3.5 and sets the trapdoor
\ - now s2 toggles led1
\ - the SVSH interrupt triggers the final save of the epoch counter
\  

eraseflash
compiletoflash

include ../cornerstone-fram.txt  
\
\ When using lpm3.5 across different modules the following technique
\ is used to separate the different initialisation codes:
\
\ - init is used for normal mode startup
\ - lpminit is used port and module configuration at lmp3.5 sleep and startup
\ - lpmint is uset to enable interrupts after lpm3.5 sleep and startup 
\
\ The lpm init and sleep main code is found in the lmp35.fs module
: init ( -- )  \ init placeholder for cold startup configuration
   cr ." ...head of init..."
;
\
: lpminit ( -- )  \ init placeholder for lpm3.5 startup configuration
;
\
: lpmint ( -- )  \ init placeholder for lpm3.5 interrupt startup configuration
;
\
\ The teston and testof modules are used to run debug test code
\ during code dowload.  The test code is compiled to ram and
\ forgetram used to clear the command after testing.
\
include teston.txt
\ include testoff.txt
\
\  setupdebug is used for stack checking and dumping ram
\  ( return stack checking can be added )
\
include setupdebug.fs
\
\ Constants
\ ---------
\
\ It is included here to simplify code.
\
include setconstants.fs
\
\ The port setup includes the Launchpad switches and leds as well as the serial port.
\
include setupports.fs
\
\ extended memory utilities
include xmem.fs
\ lfxt  and rtc setup
include setuprtc.fs                    
\ for epoch 32-bit counter.
include setupepoch.fs
\ main lpm5 stuff
include lpm5.fs

: eraseflash ( -- )  
  true TRAPDOOR !            \ cancel trapdoor 
  eraseflash                 \ chain
;
cornerstone <<lpm3.5>>

: init  
  init cr ." ...tail of init..." cr
  ['] toggle1 hook-s2 !
  ['] toggle2 hook-s1 !
;

