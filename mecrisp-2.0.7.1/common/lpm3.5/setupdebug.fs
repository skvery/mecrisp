\ setupdebug.fs
\
: check ( -- ) \ check for some things like stack depth, etc
  depth if h.s rdepth 5 - ." rStack: [" . ." ]" then
  rdepth 5 - if ." rStack: [" rdepth 5 - . ." ]" cr h.s then
;

check

: showbase ( BASE -- ) \ print 16 words from BASE
  cr dup hex.
    dup $10 +
  swap ?do cr i hex. ." : " i @ hex. 2 +loop
  cr
  cr
;

: showchar ( BASE -- ) \ print 16 bytes from BASE
  cr dup hex.
    dup $10 +
  swap ?do cr i hex. ." : " i c@ hex. 1 +loop
  cr
  cr
;

: shownx ( daddr n -- ) \ print n words from daddr
  3 rshift 0     ( daddr n/8 0 )
  do
    cr 
    2dup i 8 *     0 d+ hex. hex. ."  : " 
    2dup i 8 *     0 d+ x@ hex. 
    2dup i 8 * 2+  0 d+ x@ hex. 
    ."  " 
    2dup i 8 * 4 + 0 d+ x@ hex. 
    2dup i 8 * 6 + 0 d+ x@ hex. 
  loop 
  2drop
  cr
  cr
;

: showx ( daddr -- ) \ print 16 words from daddr
  $20 shownx
;

: -showx ( daddr -- ) \ print 16 words back from daddr
  
  $20 0 d- 
  0 $20 - 0 
  do 
    cr 2dup i 0 d- hex. hex. ."  : " 2dup i 0 d- x@ hex. 
    -2 
  +loop
  2drop
  cr
  cr
;

check