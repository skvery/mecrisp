\ setupX0000h.fs
\ 
\ see setupx.fs line 7 for definition of actual start point of static external memory
\
\ page 0 lower 64 K
\ page 1 fram 64 K reserved as profile memory
\ page 2, onwards, fram to be used for buffers
\ (Remember to arrange allotments from large to small!)
\
\
$



\ reserve page one (and two)
32K xalign,
32K xallot
32K xallot
t{ next.piece 2@ -> 0 2 }t

check
: init init
;