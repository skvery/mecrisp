\ lpm5.fs
\
\ Low Power Mode 3.5
\ 
\ Here follows the complicated sleep and init routines
\
\ The following interrupts events will cause a reset of the device.  
\ This reset might be before or after a complete power down of the system and 
\ the cause can be read from SYSRSTIV.
\
\ $02 Brownout reset
\ $04 RSTIFG                 \ reset pin
\ $06 PMMSWBOR
\ $08 LPMx.5 wake up
\ $0A Security violation
\ $0C Reserved
\ $0E SVSHIFG
\ $10 Reserved
\ $12 Reserved
\ $14 PMMSWPOR
\ $16 WDTIFG
\ $18 WDTPW                  \ used by Mecrisp reset
\ . 
\ . 
\ .
\  PMMLPM5IFG $8000 (.15) 
\     SVSHIFG $2000 (.13) 
\ PMMSWPORIFG $0400 (.10) 
\ PMMRSTIFG   $0200 (.9) and 
\ PMMSWBORIFG $0100 (.8) can be read \ from the PMMIFG register.
\
\ This means that the RTC counter registers:
\
\ must be saved under -
\ $0E from SYSRSTIV indicating power down.
\
\ will be unreliable under -
\ any BOR
\
$1800 constant TRAPDOOR                \ trapdoor hook defined inside Mecrisp for fast wakeup

: swbor ( -- na )                      \ software defined BOR with trapdoor bypass
  true TRAPDOOR !                      \ reset trapdoor first
  PWD_PMM PMMPW PMMBase + c!           \ unlock PMM
  PMMSWBOR PMMCTL0 PMMBase + cbis!     \ BOR
  0       PMMPW PMMBase + c!           \ lock PMM
;
: lpm5 ( -- na )                       \ lpm5 power off
  [ $D072 , $00F0 , ]                  \ BIS.B #CPUOFF+OSCOFF+SCG0+SCG1, SR
  inline
; 
: lpmsleep ( -- )                      \ Final preparation to enter lpm5 
\  verbose @ if ."  going to sleep now..." cr then
\  verbose @ if begin SYSRSTIV c@ dup ." SYSRSTIV = $" hex. cr 0= until then 
\  verbose @ if cr ." ...lpm5.fs low power sleep, disable interrupts  " then
  lpminit
  lpmint
  nop                                  \ finish ints
  dint                                 \ clear GIE bit
  PWD_PMM PMMPW PMMBase + c!           \ unlock PMM
  PMMREGOFF PMMCTL0 PMMBase + cbis!    \ PMMREGOFF
  0       PMMPW PMMBase + c!           \ lock PMM
  lpm5                                 \ Go to sleep
  cr ." nogo area - lpmsleep not chained correctly?" cr
;
: wake-up ( -- )                       \ init placeholder for trapdoor startup configuration
  PMMIFG PMMBase + @ $1810 TEMP @ + !  \ 
  2 TEMP +!
  SYSRSTIV @ dup TEMP @ $1810 + !      \ read SYSRSTIV 
  case
    $0E of                             \ SVSH = $0E
\     LOCKLPM5 PM5CTL0 PMMBase + bic!  \ clear LOCKLPM5
      RTCCNT12 RTCBASE + @ Epoch0-LL ! \ Epoch0-LL = RTCCNT12
      RTCCNT34 RTCBASE + @ Epoch0-HH ! \ Epoch0-HH = RTCCNT34
      true TRAPDOOR !                  \ clear trapdoor
      2 TEMP +!                        \ inc temp
      begin dup sec? = until           \ wait 256 s for bor
      swbor ( -- na )                  \ terminate program flow
    endof
    $08 of                    \ LPMx.5 woke 
      lpminit
      LOCKLPM5 PM5CTL0 PMMBase + bic!   \ clear LOCKLPM5
\      toggle1
      eint
      lpmint
      ( do what is needed )   \ execute task required -
     \  verbose @ if cr ." !" cr then
      2 TEMP +!
       lpmsleep ( -- na )      \ terminate program flow
    endof
    loadEpoch
    2 TEMP +!
  endcase
  true TRAPDOOR !            \ clear trapdoor
  reset                      \ normal reset 
;
: init
  init
  verbose @ if cr ." ...lpm5.fs init, enable interrupts" then
  eint
  verbose @ if begin SYSRSTIV c@ dup ."  trapdoor activation with SYSRSTIV = $" hex. cr 0= until then
  ['] wake-up TRAPDOOR !            \ Actvate reboot trapdoor
;