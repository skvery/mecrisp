\ setupepoch.fs

\ The rtc is configured as main epoch ( 130 year ) counter.
\ The svsh flag is used to detect power down reset.
\ We will need a way to ensure that writing epoch values are not corrupted.
\ This is done by writing a second copy and tagging it with a check of the low word.

\ Define epoch counter backup in fram information memory

$180C constant EPOCH0-LL   \ epoch per second running timer backup that cannot be adjusted
$180A constant EPOCH0-HH   \  
$1808 constant EPOCH-CC    \ time check only valid if -LL = -CC see backup
$1806 constant TEMP        \ placeholder
$1804 constant EPOCH1-LL   \ use as second backup
$1802 constant EPOCH1-HH 

: copyEpoch0  ( -- )         \ copy Epoch0 to Epoch1
  Epoch0-HH 2@ Epoch1-HH 2! 
;
: copyEpoch1  ( -- )         \ copy Epoch1 to Epoch0
  Epoch1-HH 2@ Epoch0-HH  2! 
  Epoch1-LL  @ Epoch-CC    !
;
: sec? ( -- u )            \ read 16-bit count as soon as it is stable
  RTCCNT1 RTCBASE + @
  begin
    RTCCNT1 RTCBASE + @
    dup 
    rot
    =
  until
;
: 2sec? ( -- ud )           \ read 32-bit counter until the last two readings are the same
  RTCCNT1 RTCBASE + 2@
  begin
    RTCCNT1 RTCBASE + 2@
    2dup 
    2rot
    d=
  until
  swap                      \ word sequence changed to mecrisp forth.
;
: capture2Epoch ( -- )     \ copy rtc counter to epoch
      RTCCNT1 RTCBASE + dup              \ 
      2@ swap Epoch0-HH 2!               \
      dup @ Epoch-CC !                   \ save epoch counter
      2@ swap Epoch1-HH 2!               \
  inline
;
: loadEpoch ( -- )
  Epoch0-LL @ Epoch-CC @ = if copyEpoch0 else copyEpoch1 then \ use backup if corruption detected
  dint
  PWD_RTC RTCBASE RTCPWD + c!          \ unlock rtc
  RTCHOLD RTCBASE RTCCTL1 + cbis!      \ Set the RTCHOLD bit to stop the counters.
  Epoch0-HH 2@ swap RTCCNT1 RTCBASE + 2!    
  RTCHOLD RTCBASE RTCCTL1 + cbic!      \ 4. Clear the RTCHOLD bit to start the counters.
  0 RTCBASE RTCPWD + c!                \ lock rtc
  eint
;
