\ setupports.fs
\
\ For the MSP430FR5994 Launchpad:
\
\ P4.0 input OUT=1 for low current and other SD card pins are unused
\ P5.5, P5.6 switches s2, s1 need pullup
\ P2.0, P2.1 (tx and rx) uses SEL0 and SEL1
\
\ P1.0, P1.1 leds s1, s2 configured as output
\ (use jumper to disable led1 for energy++ testing)
\
\ s1 interrupt is used to enter sleep mode
\ s2 interrupt toggle1 in both active and sleep modes
\ rx interrupt is used to exit sleep mode
: toggle1 ( -- )              \ Toggles P1.0
  .0 PxOUT P1Base + cxor!
;
: toggle2 ( -- )              \ Toggles P1.1
  .1 PxOUT P1Base + cxor!
;
: on1 ( -- )
  .0 PxOUT P1Base + cbis!
;
: on2 ( -- )
  .1 PxOUT P1Base + cbis!
;
: off1 ( -- )
  .0 PxOUT P1Base + cbic!
;
: off2 ( -- )
  .1 PxOUT P1Base + cbic!
;
: s1? ( -- flag )             \ S1 active?
  .6 PxIN P5Base + cbit@ not
;
: s2? ( -- flag )             \ S2 active?
  .5 PxIN P5Base + cbit@ not
;

' nop variable hook-rx        \ in case we want to use this to wake up
' nop variable hook-s1
' nop variable hook-s2 

: port2-isr ( -- )
  P2IV c@
  case
    $04 of  \ .1 - rx
      hook-rx @ execute
    endof
  endcase
;   
: port5-isr ( -- )
  P5IV c@
  case
    $0C of  \ .5 - s2
      hook-s2 @ execute
    endof
    $0E of  \ .6 - s1
      hook-s1 @ execute
    endof
  endcase
;   
: linitports ( -- ) 
\ Set all unused ports input with pulldown
\ all changes from reset mode required again
\ LFXT settings update done in setuprtc.fs

  .4      PxSEL0 PJBase + c!  \ PJ SEL0 lfxt

\ .0 .1 + PxSEL1 P2Base + cbic!         \ P2.0, P2.1 SEL1 = 0 SEL0 = 0 tx rx off default
  
  .8 .9 + PxOUT  P1Base + !   \ P1, P2 clear P2.0, P2.1 pullup
  .8      PxOUT  P3Base + !   \ P3, P4 clear P4.0 pullup
  .5 .6 + PxOUT  P5Base + !   \ P5, P6 clear P5.5, P5.6 pullup

  .0 .1 + PxDIR  P1Base + c!  \ P1.0 and P1.1 to LED output direction

  true .9 xor PxREN  P1Base + !          \ P1, P2 enable. P2.1 (rx) no REN with jumper 
  true    PxREN  P3Base + !   \ P3, P4 enable
  true    PxREN  P5Base + !   \ P5, P6 enable
  true    PxREN  P7Base + !   \ P7, P8 enable
  true    PxREN  PJBase + c!  \ PJ enable
;
: initports ( -- )            \ Set all ports input with pulldown
\ 0       PxSEL0 P1Base + !   \ P1, P2 SEL0
\ 0       PxSEL0 P3Base + !   \ P3, P4 SEL0
\ 0       PxSEL0 P5Base + !   \ P5, P6 SEL0
\ 0       PxSEL0 P7Base + !   \ P7, P8 SEL0
  .4      PxSEL0 PJBase + c!  \ PJ SEL0 lfxt
  
\ 0       PxSEL1 P1Base + !   \ P1, P2 SEL1
  .0 .1 + PxSEL1 P2Base + c!  \ P2.0, P2.1 SEL1 = 1 SEL0 = 0 (tx rx)
\ 0       PxSEL1 P3Base + !   \ P3, P4 SEL1
\ 0       PxSEL1 P5Base + !   \ P5, P6 SEL1
\ 0       PxSEL1 P7Base + !   \ P7, P8 SEL1
\ 0       PxSEL1 PJBase + c!  \ PJ SEL1

\ 0       PxOUT  P1Base + !   \ P1, P2 clear
  .8      PxOUT  P3Base + !   \ P3, P4 clear P4.0 pullup
  .5 .6 + PxOUT  P5Base + !   \ P5, P6 clear P5.5, P5.6 pullup
\ 0       PxOUT  P7Base + !   \ P7, P8 clear 
\ 0       PxOUT  PJBase + c!  \ PJ input clear

.0 .1 + PxDIR  P1Base + !   \ P1, P2 clear P1.0 and P1.1 to LED output direction
\ 0       PxDIR  P3Base + !   \ P3, P4 input
\ 0       PxDIR  P5Base + !   \ P5, P6 input
\ 0       PxDIR  P7Base + !   \ P7, P8 input
\ 0       PxDIR  PJBase + c!  \ PJ input

  true    PxREN  P1Base + !   \ P1, P2 enable
  true    PxREN  P3Base + !   \ P3, P4 enable
  true    PxREN  P5Base + !   \ P5, P6 enable
  true    PxREN  P7Base + !   \ P7, P8 enable
  true    PxREN  PJBase + c!  \ PJ ENABLE
;
: lportints ( -- )            \ lpm interrupts

  .1 PxIES P2Base + cbis!     \ select H -> L 
  .1 PxIE  P2Base + cbis!     \ enable

  .5 PxIES P5Base + cbis!     \ select H -> L 
  .5 PxIE  P5Base + cbis!     \ enable
  .6 PxIES P5Base + cbis!     \ select H -> L 
  .6 PxIE  P5Base + cbis!     \ enable
;
: init init         \ chain
  initports
  ['] port2-isr irq-port2 !   \ setup isr
  ['] port5-isr irq-port5 !   \ setup isr
  lportints
;
: lpmint lpmint     \ chain
  lportints
;
: lpminit lpminit   \ chain
  linitports
;
