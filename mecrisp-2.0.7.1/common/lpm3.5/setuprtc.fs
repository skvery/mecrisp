\ setuprtc.fs

\ PJSEL0.4 and PJSEL0.5 = 1
\ PJSEL1.4 and PJSEL1.5 = 0
\ LFXT BYPASS = 0

\ RTCMODE = 0 counter mode and using 1 s ticks
\ RTCTEV = $00 event on minute changed

\ The RTCPWD register implements key protection and controls the lock or unlock state of the module.
\ When this register is written with correct key, 0A5h, the module is unlocked and unlimited write access
\ possible to RTC_C registers. After the module is unlocked, it remains unlocked until the user writes any
\ incorrect key or until the module is reset.

\ RTCPS0CTL
\ .13 .12 + .11 + constant RT0PSDIV       \ 13-11  111 = Divide by 256

\ RTCPS1CTL
\ .15       constant RT1SSEL              \ 15-14 1x SSEL RT0PS
\ .13 .12 + constant RT1PSDIV             \ 13-11  110 = Divide by 128
\ .4 .3 +   constant RT1IP                \ 4-2 RT1IPx RW 0h 110b = Divide by 128 for 1 Hz
\ .1        constant RT1PSIE              \ 1 RT1PSIE RW 0h 

\ RTCCTL1
\ .6 constant RTCHOLD           \ 6 RTCHOLD RW 1 for hold 
\ .5 constant RTCMODE           \ 5 RTCMMODE RW 1 for calendar 
\ .3 constant RTCSSEL           \ 3-2 1x SSEL RT1PS

\ Real-Time Clock Interrupt Vector Register
\ 0Eh = Interrupt Source: RTC prescaler 1; Interrupt Flag: RT1PSIFG
\ For interrupt handler refer to 23.2.6.1 RTCIV Software Example

\ Enter LPM3.5 as follows:
\
\ bic #RTCHOLD, &RTCCTL13
\ bis #PMMKEY + REGOFF, &PMMCTL0
\ bis #LPM4, SR

\ 4. An LPM3.5 wake-up event like an edge on a wake-up input pin or an RTC_C interrupt event starts the
\ BOR entry sequence and the core voltage regulator. All peripheral registers are set to their default
\ conditions. The I/O pin state and the interrupt configuration for the RTC_C remain locked.
\ 5. The device can be configured. The I/O configuration and the RTC_C interrupt configuration that was
\ not retained during LPM3.5 should be restored to the values that they had before entering LPM3.5.
\ Then the LOCKLPM5 bit can be cleared, which releases the I/O pin conditions and the RTC_C
\ interrupt configuration. Registers that are retained during LPM3.5 should not be altered before
\ LOCKLPM5 is cleared.
\ 6. After enabling I/O and RTC_C interrupts, the interrupt that caused the wake-up can be serviced.

: rtc-isr ( -- )              \ one second interrupt not used for saving epoch                          
  0 RTCBASE RTCIV + !         \ write interrupt vector to reset all flags 
  toggle1                     \ from utility toggle LED 
;
: initrtci ( -- )                       \ init with interrupt
  verbose @ if cr ." ...setuprtc init, arm rtc-isr" then
  dint
  PWD_RTC RTCBASE RTCPWD + c!           \ unlock
  RT1PSIE RTCBASE  RTCPS1CTL + cbis!    \ enable interrupt
  0 RTCBASE RTCPWD + c!                 \ lock
  eint
;
: initrtc ( -- )                        \ init without interrupt
  dint     
  PWD_RTC  RTCBASE RTCPWD  + c!         \ unlock
  RTCSSEL  RTCBASE RTCCTL1 + c!         \ setup counter mode.
  RT0PSDIV RTCBASE RTCPS0CTL + !
  RT1SSEL RT1PSDIV + RT1IP + RTCBASE RTCPS1CTL + !
  RTCHOLD RTCBASE RTCCTL1 + cbic!       \ clear the RTCHOLD and bit to start the counter.
  0 RTCBASE RTCPWD + c!                 \ lock
  eint
;
: init init         \ chain
  initrtc
  ['] rtc-isr irq-rtc !                 \ setup isr
  initrtci
;
: lpmint lpmint     \ chain
  initrtci
;
: lpminit lpminit   \ chain
  initrtc
;
