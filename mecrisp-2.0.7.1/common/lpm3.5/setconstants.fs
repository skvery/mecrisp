\ ************************
\ setcontstants.fs
\ ************************

$01 constant .0
$02 constant .1
$04 constant .2
$08 constant .3
$10 constant .4
$20 constant .5
$40 constant .6
$80 constant .7

$0100 constant .8
$0200 constant .9
$0400 constant .10
$0800 constant .11
$1000 constant .12
$2000 constant .13
$4000 constant .14
$8000 constant .15

 $400 constant  1K
 $800 constant  2K 
$1000 constant  4K 
$2000 constant  8K
$4000 constant 16K 
$8000 constant 32K
$10000. 2constant 64K           \ maximum word size           

\ ASCII control characters
  00 constant _NULL ( Null character  )
  01 constant _SOH  ( Start of Header )
  02 constant _STX  ( Start of Text   )
  03 constant _ETX  ( End of Text     )
  04 constant _EOT  ( End of Trans.   )
  05 constant _ENQ  ( Enquiry         )
  06 constant _ACK  ( Acknowledgement )
  07 constant _BEL  ( Bell            )
  08 constant _BS   ( Backspace       )
  09 constant _HT   ( Horizontal Tab  )
  10 constant _LF   ( Line Feed       )
  11 constant _VT   ( Vertical Tab    )
  12 constant _FF   ( Form feed       )
  13 constant _CR   ( Carriage Return )
  14 constant _SO   ( Shift Out       )
  15 constant _SI   ( Shift In        )
  16 constant _DLE  ( Data Link Escape)
  17 constant _DC1  ( Device Control 1)
  18 constant _DC2  ( Device control 2)
  19 constant _DC3  ( Device control 3)
  20 constant _DC4  ( Device control 4)
  21 constant _NAK  ( Negative Acknowl.)
  22 constant _SYN  ( Synchronous Idle)
  23 constant _ETB  ( End of Trans. Block)
  24 constant _CAN  ( Cancel             )
  25 constant _EM   ( End of Medium      )
  26 constant _SUB  ( Substitute         )
  27 constant _ESC  ( Escape)
  28 constant _FS   ( File separator)
  29 constant _GS   ( Group separator)
  30 constant _RS   ( Record separator)
  31 constant _US   ( Unit separator)
  32 constant _SP   ( Space)
  33 constant _!
  34 constant _"
  35 constant _#
  36 constant _$
  37 constant _%
  38 constant _&
  39 constant _'
  40 constant _(
  41 constant _)
  42 constant _*
  43 constant _+
  44 constant _,
  45 constant _-
  46 constant _.
  47 constant _/
[char] 0 constant _0
[char] 1 constant _1
[char] 2 constant _2
[char] 3 constant _3
[char] 4 constant _4
[char] 5 constant _5
[char] 6 constant _6
[char] 7 constant _7
[char] 8 constant _8
[char] 9 constant _9
[char] M constant _M
  127 constant DEL

$0200 constant P1Base       \ P1, P2 Registers (Base Address: 0200h)
  $020E constant P1IV            \ Port Px interrupt vector word P1357IV 0Eh
$0201 constant P2Base       \ P1, P2 Registers (Base Address: 0200h)
  $021E constant P2IV            \ Port Px interrupt vector word P2468IV 1Eh
$0220 constant P3Base       \ P3, P4 Registers (Base Address: 0220h)
  $022E constant P3IV            \ Port Px interrupt vector word P1357IV 0Eh
$0221 constant P4Base       \ P3, P4 Registers (Base Address: 0220h)
  $023E constant P4IV            \ Port Px interrupt vector word P2468IV 1Eh
$0240 constant P5Base       \ P5, P6 Registers (Base Address: 0240h)
  $024E constant P5IV            \ Port Px interrupt vector word P1357IV 0Eh
$0241 constant P6Base       \ P5, P6 Registers (Base Address: 0240h)
  $025E constant P6IV            \ Port Px interrupt vector word P2468IV 1Eh
$0260 constant P7Base       \ P7, P8 Registers (Base Address: 0260h)
  $026E constant P7IV            \ Port Px interrupt vector word P1357IV 0Eh
$0261 constant P8Base       \ P7, P8 Registers (Base Address: 0260h)
  $27E constant P8IV            \ Port Px interrupt vector word P2468IV 1Eh
$0320 constant PJBase       \ PJ Registers (Base Address: 0320h)

  $00 constant PxIN            \ Port Px input P1IN 00h
  $02 constant PxOUT           \ Port Px output P1OUT 02h
  $04 constant PxDIR           \ Port Px direction P1DIR 04h
  $06 constant PxREN           \ Port Px resistor enable P1REN 06h
  $0A constant PxSEL0          \ Port Px selection 0 P1SEL0 0Ah
  $0C constant PxSEL1          \ Port Px selection 1 P1SEL1 0Ch
  $16 constant PxSELC          \ Port Px complement selection P1SELC 16h
  $18 constant PxIES           \ Port Px interrupt edge select P1IES 18h
  $1A constant PxIE            \ Port Px interrupt enable P1IE 1Ah
  $1C constant PxIFG           \ Port Px interrupt flag P1IFG 1Ch

$0120 constant PMMBase
  $A5 constant PWD_PMM
  .15 constant PMMLPM5IFG
  .13 constant PMMSVSHIFG
  .8  constant PMMBORIFG
  .2  constant PMMSWBOR
  .3  constant PMMSWPOR
  .4  constant PMMREGOFF

  $00 constant PMMCTL0
  $01 constant PMMPW
  $0A constant PMMIFG
  $10 constant PM5CTL0

  .0  constant LOCKLPM5


$015C constant WDTBase
  $00 constant WDTCTL

$0160 constant CSBase
  $A5 constant CSKEY
  $04 constant CSCTL2
  $01 constant CSLOCK 

$019E constant SYSRSTIV


$04A0 constant RTCBASE        \ RTC_C Registers (Base Address: 04A0h)

  $A5 constant PWD_RTC

  $00 constant RTCCTL0        \ RTC control 0 RTCCTL0 00h
  $01 constant RTCPWD         \ RTC password RTCPWD 01h
  $02 constant RTCCTL1        \ RTC control 1 RTCCTL1 02h
    .6 constant RTCHOLD                 \ 6 RTCHOLD RW 1 for hold 
    .5 constant RTCMODE                 \ 5 RTCMMODE RW 1 for calendar 
    .3 constant RTCSSEL                 \ 3-2 1x SSEL RT1PS

  $03 constant RTCCTL3        \ RTC control 3 RTCCTL3 03h
  $04 constant RTCOCAL        \ RTC offset calibration RTCOCAL 04h
  $06 constant RTCTCMP        \ RTC temperature compensation RTCTCMP 06h
  $08 constant RTCPS0CTL      \ RTC prescaler 0 control RTCPS0CTL 08h
    .13 .12 + .11 + constant RT0PSDIV   \ 13-11  111 = Divide by 256

  $0A constant RTCPS1CTL      \ RTC prescaler 1 control RTCPS1CTL 0Ah
    .15       constant RT1SSEL          \ 15-14 1x SSEL RT0PS
    .13 .12 + constant RT1PSDIV         \ 13-11  110 = Divide by 128
    .4 .3 +   constant RT1IP            \ 4-2 RT1IPx RW 0h 110b = Divide by 128 for 1 Hz
    .1        constant RT1PSIE          \ 1 RT1PSIE RW 0h 

  $0C constant RTCPS0           \ RTC prescaler 0 RTCPS0 0Ch
  $0D constant RTCPS1           \ RTC prescaler 1 RTCPS1 0Dh
  $0E constant RTCIV            \ RTC interrupt vector word RTCIV 0Eh
  $10 constant RTCCNT12         \ RTC seconds/counter 12 RTCSEC/RTCNT1 10h
  $10 constant RTCCNT1          \ RTC seconds/counter 1 RTCSEC/RTCNT1 10h
  $11 constant RTCCNT2          \ RTC minutes/counter 2 RTCMIN/RTCNT2 11h
  $12 constant RTCCNT34         \ RTC hours/counter 34 RTCHOUR/RTCNT3 12h
  $12 constant RTCCNT3          \ RTC hours/counter 3 RTCHOUR/RTCNT3 12h
  $13 constant RTCCNT4          \ RTC day of w/counter 4 RTCDOW/RTCNT4 13h
  $14 constant RTCDAY           \ RTC days RTCDAY 14h
  $15 constant RTCMON           \ RTC month RTCMON 15h
  $16 constant RTCYEAR          \ RTC year RTCYEAR 16h
  $18 constant RTCAMIN          \ RTC alarm minutes RTCAMIN 18h
  $19 constant RTCHOUR          \ RTC alarm hours RTCAHOUR 19h
  $1A constant RTCADOW          \ RTC alarm day of week RTCADOW 1Ah
  $1B constant RTCADAY          \ RTC alarm days RTCADAY 1Bh
  $1C constant BIN2BCD          \ Binary-to-BCD conversion BIN2BCD 1Ch
  $1E constant BCD2BIN          \ BCD-to-binary conversion BCD2BIN 1Eh

$03C0 constant TB0Base    \ TB0 Registers (Base Address: 03C0h)
  $00 constant T_CTL         \ control T_CTL 00h
  $10 constant T_R           \ register T_R 10h
  $12 constant T_CCR0        \ counter re-load value T_CCR0 $12
  $20 constant T_EX0         \ TB0 expansion 0 TB0EX0 20hS 

$0000 constant MC_RESET
$0100 constant TBSSEL       \ ACLK
$0004 constant TBCLR         
$0010 constant MC_UM        \ up mode
$0020 constant MC_CM        \ continuous mode
$00C0 constant ID8          \ divider = 8 
$0080 constant ID4          \ divider = 4 
$0040 constant ID2          \ divider = 2
$0000 constant ID1          \ divider = 1
7 constant TBIDEX8          \ divide extend = 8 

$0510 constant DMA0Base   \ Channel 0: 0510h, 
$0520 constant DMA1Base   \ Channel 1: 0520h, 
$0530 constant DMA2Base   \ Channel 2: 0530h,
$0540 constant DMA3Base   \ Channel 3: 0540h, 
$0550 constant DMA4Base   \ Channel 4: 0550h, 
$0560 constant DMA5Base   \ Channel 5: 0560h,

$0500 constant DMACTL0     \ DMA module control 0 DMACTL0 00h
$0502 constant DMACTL1     \ DMA module control 1 DMACTL1 02h
$0504 constant DMACTL2     \ DMA module control 2 DMACTL2 04h
$0506 constant DMACTL3     \ DMA module control 3 DMACTL3 06h
$0508 constant DMACTL4     \ DMA module control 4 DMACTL4 08h
$050E constant DMACIV      \ DMA interrupt vector DMAIV 0Eh

  $00 constant DMAxCTL       \ DMA channel x control DMA0CTL 00h
  $02 constant DMAxSA        \ DMA channel x source address low DMA0SAL 02h
  $06 constant DMAxDA        \ DMA channel x destination address low DMA0DAL 06h
  $0A constant DMAxSZ        \ DMA channel x transfer size DMA0SZ 0Ah

   .4 constant DMAEN
   .3 constant DMAIFG
   .0 constant DMAREQ

\ serial
$05C0 constant eUSCIA0Base       \ eUSCI_A0 Registers (Base Address: 05C0h)
$05E0 constant eUSCIA1Base       \ eUSCI_A1 Registers (Base Address: 05E0h)
$0600 constant eUSCIA2Base       \ eUSCI_A2 Registers (Base Address: 0600h)
$0620 constant eUSCIA3Base       \ eUSCI_A3 Registers (Base Address: 0620h)

  $00 constant CTLW0          \ eUSCI_A control word 0          UCA2CTLW0   00h
  $02 constant CTLW1          \ eUSCI_A control word 1          UCA2CTLW1   02h
  $06 constant BRW            \ eUSCI_A baud rate word          UCA2BR0     06h
  $08 constant MCTLW          \ eUSCI_A modulation control      UCA2MCTLW   08h
  $0A constant STATW          \ eUSCI_A status word             UCA2STATW   0Ah
  $0C constant RXBUF          \ eUSCI_A receive buffer          UCA2RXBUF   0Ch
  $0E constant TXBUF          \ eUSCI_A transmit buffer         UCA2TXBUF   0Eh
  $10 constant ABCTL          \ eUSCI_A LIN control             UCA2ABCTL   10h
  $12 constant IRTCTL         \ eUSCI_A IrDA transmit control   UCA2IRTCTL  12h
  $13 constant IRRCTL         \ eUSCI_A IrDA receive control    UCA2IRRCTL  13h
  $1A constant IE             \ eUSCI_A interrupt enable        UCA2IE      1Ah
  $1C constant IFG            \ eUSCI_A interrupt flags         UCA2IFG     1Ch
  $1E constant IV             \ eUSCI_A interrupt vector word   UCA2IV      1Eh

   .3 constant UCTXCPTIFG      \ Transmit complete interrupt flag. 
   .2 constant UCSTTIFG        \ Start bit interrupt flag.
   .1 constant UCTXIFG         \ Transmit interrupt flag. UCTXIFG is set when UCAxTXBUF empty.
   .0 constant UCRXIFG         \ Receive interrupt flag. UCRXIFG is set when UCAxRXBUF is ready.

$FFEE constant eUSCI-A0     \ Interrupt maskable 0FFEEh high
$FFE6 constant eUSCI-A1     \ Interrupt maskable 0FFEEh 
$FFC2 constant eUSCI-A2     \ Interrupt maskable 0FFC2h 
$FFC0 constant eUSCI-A3     \ Interrupt maskable 0FFC0h low

$01B0 constant REFCTL        \ Shared Reference Register (Base Address: 01B0h)

$00 constant REFVSEL12       \ 00b = 1.2 V available when reference requested or REFON = 1
$10 constant REFVSEL20       \ 01b = 2.0 V available when reference requested or REFON = 1
$20 constant REFVSEL25       \ 10b = 2.5 V available when reference requested or REFON = 1

$05A0 constant MPUBase       \ MPU Control Registers (Base Address: 05A0h)

$00 constant MPUCTL0         \ MPU control 0 MPUCTL0 00h
$02 constant MPUCTL1         \ MPU control 1 MPUCTL1 02h
$04 constant MPUSEGB2        \ MPU segmentation border 2 MPUSEGB2 04h
$06 constant MPUCSEGB1       \ MPU segmentation border 1 MPUSEGB1 06h
$08 constant MPUSAM          \ MPU access management MPUSAM 08h
$0A constant MPUIPC0         \ MPU IP control 0 MPUIPC0 0Ah
$0C constant MPUIPSEGB2      \ MPU IP encapsulation segment border 2 MPUIPSEGB2 0Ch
$0E constant MPUIPSEGB1      \ MPU IP encapsulation segment border 1 MPUIPSEGB1 0Eh

$0800 constant ADC12Base     \ ADC12_B Registers (Base Address: 0800h)

$00 constant ADC12CTL0       \ ADC12_B control 0 ADC12CTL0 00h
.0      constant ADC12SC
.1      constant ADC12ENC
.4      constant ADC12ON
.7      constant ADC12MSC
.9 .8 + constant ADC12SHT0_32

$02 constant ADC12CTL1       \ ADC12_B control 1 ADC12CTL1 02h
.9      constant ADC12SHP
 0      constant ADC12SSEL_MODOSC
.3      constant ADC12SSEL_ALK
.4      constant ADC12SSEL_MCLK 
.4 .3 + constant ADC12SSEL_SMCLK
.2      constant ADC12CONSEQ2
.1      constant ADC12CONSEQ1

$04 constant ADC12CTL2       \ ADC12_B control 2 ADC12CTL2 04h
.0  constant ADC12PWRMD    

$06 constant ADC12CTL3       \ ADC12_B control 3 ADC12CTL3 06h
.7 constant ADC12TCMAP
.6 constant ADC12BATMAP

$08 constant ADC12LO         \ ADC12_B window comparator low threshold register ADC12LO 08h
$0A constant ADC12HI         \ ADC12_B window comparator high threshold register ADC12HI 0Ah
$0C constant ADC12IFGR0      \ ADC12_B interrupt flag register 0 ADC12IFGR0 0Ch
$0E constant ADC12IFGR1      \ ADC12_B interrupt flag register 1 ADC12IFGR1 0Eh
$10 constant ADC12IFGR2      \ ADC12_B interrupt flag register 2 ADC12IFGR2 10h
$12 constant ADC12IER0       \ ADC12_B interrupt enable register 0 ADC12IER0 12h
$14 constant ADC12IER1       \ ADC12_B interrupt enable register 1 ADC12IER1 14h
$16 constant ADC12IER2       \ ADC12_B interrupt enable register 2 ADC12IER2 16h
$18 constant ADC12IV         \ ADC12_B interrupt vector ADC12IV 18h
$20 constant ADC12MCTL0       \ ADC12_B memory control 0 ADC12MCTL0 20h
$22 constant ADC12MCTL1       \ ADC12_B memory control 1 ADC12MCTL1 22h
$24 constant ADC12MCTL2       \ ADC12_B memory control 2 ADC12MCTL2 24h
$26 constant ADC12MCTL3       \ ADC12_B memory control 3 ADC12MCTL3 26h
$28 constant ADC12MCTL4       \ ADC12_B memory control 4 ADC12MCTL4 28h
$2A constant ADC12MCTL5       \ ADC12_B memory control 5 ADC12MCTL5 2Ah
$2C constant ADC12MCTL6       \ ADC12_B memory control 6 ADC12MCTL6 2Ch
$2E constant ADC12MCTL7       \ ADC12_B memory control 7 ADC12MCTL7 2Eh
$30 constant ADC12MCTL8       \ ADC12_B memory control 8 ADC12MCTL8 30h
$32 constant ADC12MCTL9       \ ADC12_B memory control 9 ADC12MCTL9 32h
$34 constant ADC12MCTL10       \ ADC12_B memory control 10 ADC12MCTL10 34h
$36 constant ADC12MCTL11       \ ADC12_B memory control 11 ADC12MCTL11 36h
$38 constant ADC12MCTL12       \ ADC12_B memory control 12 ADC12MCTL12 38h
$3A constant ADC12MCTL13       \ ADC12_B memory control 13 ADC12MCTL13 3Ah
$3C constant ADC12MCTL14       \ ADC12_B memory control 14 ADC12MCTL14 3Ch
$3E constant ADC12MCTL15       \ ADC12_B memory control 15 ADC12MCTL15 3Eh
$40 constant ADC12MCTL16       \ ADC12_B memory control 16 ADC12MCTL16 40h
$42 constant ADC12MCTL17       \ ADC12_B memory control 17 ADC12MCTL17 42h
$44 constant ADC12MCTL18       \ ADC12_B memory control 18 ADC12MCTL18 44h
$46 constant ADC12MCTL19       \ ADC12_B memory control 19 ADC12MCTL19 46h
$48 constant ADC12MCTL20       \ ADC12_B memory control 20 ADC12MCTL20 48h
$4A constant ADC12MCTL21       \ ADC12_B memory control 21 ADC12MCTL21 4Ah
$4C constant ADC12MCTL22       \ ADC12_B memory control 22 ADC12MCTL22 4Ch
$4E constant ADC12MCTL23       \ ADC12_B memory control 23 ADC12MCTL23 4Eh
$50 constant ADC12MCTL24       \ ADC12_B memory control 24 ADC12MCTL24 50h
$52 constant ADC12MCTL25       \ ADC12_B memory control 25 ADC12MCTL25 52h
$54 constant ADC12MCTL26       \ ADC12_B memory control 26 ADC12MCTL26 54h
$56 constant ADC12MCTL27       \ ADC12_B memory control 27 ADC12MCTL27 56h
$58 constant ADC12MCTL28       \ ADC12_B memory control 28 ADC12MCTL28 58h
$5A constant ADC12MCTL29       \ ADC12_B memory control 29 ADC12MCTL29 5Ah
$5C constant ADC12MCTL30       \ ADC12_B memory control 30 ADC12MCTL30 5Ch
$5E constant ADC12MCTL31       \ ADC12_B memory control 31 ADC12MCTL31 5Eh
$60 constant ADC12MEM0       \ ADC12_B memory 0 ADC12MEM0 60h
$62 constant ADC12MEM1       \ ADC12_B memory 1 ADC12MEM1 62h
$64 constant ADC12MEM2       \ ADC12_B memory 2 ADC12MEM2 64h
$66 constant ADC12MEM3       \ ADC12_B memory 3 ADC12MEM3 66h
$68 constant ADC12MEM4       \ ADC12_B memory 4 ADC12MEM4 68h
$6A constant ADC12MEM5       \ ADC12_B memory 5 ADC12MEM5 6Ah
$6C constant ADC12MEM6       \ ADC12_B memory 6 ADC12MEM6 6Ch
$6E constant ADC12MEM7       \ ADC12_B memory 7 ADC12MEM7 6Eh
$70 constant ADC12MEM8       \ ADC12_B memory 8 ADC12MEM8 70h
$72 constant ADC12MEM9       \ ADC12_B memory 9 ADC12MEM9 72h
$74 constant ADC12MEM10       \ ADC12_B memory 10 ADC12MEM10 74h
$76 constant ADC12MEM11       \ ADC12_B memory 11 ADC12MEM11 76h
$78 constant ADC12MEM12       \ ADC12_B memory 12 ADC12MEM12 78h
$7A constant ADC12MEM13       \ ADC12_B memory 13 ADC12MEM13 7Ah
$7C constant ADC12MEM14       \ ADC12_B memory 14 ADC12MEM14 7Ch
$7E constant ADC12MEM15       \ ADC12_B memory 15 ADC12MEM15 7Eh
$80 constant ADC12MEM16       \ ADC12_B memory 16 ADC12MEM16 80h
$82 constant ADC12MEM17       \ ADC12_B memory 17 ADC12MEM17 82h
$84 constant ADC12MEM18       \ ADC12_B memory 18 ADC12MEM18 84h
$86 constant ADC12MEM19       \ ADC12_B memory 19 ADC12MEM19 86h
$88 constant ADC12MEM20       \ ADC12_B memory 20 ADC12MEM20 88h
$8A constant ADC12MEM21       \ ADC12_B memory 21 ADC12MEM21 8Ah
$8C constant ADC12MEM22       \ ADC12_B memory 22 ADC12MEM22 8Ch
$8E constant ADC12MEM23       \ ADC12_B memory 23 ADC12MEM23 8Eh
$90 constant ADC12MEM24       \ ADC12_B memory 24 ADC12MEM24 90h
$92 constant ADC12MEM25       \ ADC12_B memory 25 ADC12MEM25 92h
$94 constant ADC12MEM26       \ ADC12_B memory 26 ADC12MEM26 94h
$96 constant ADC12MEM27       \ ADC12_B memory 27 ADC12MEM27 96h
$98 constant ADC12MEM28       \ ADC12_B memory 28 ADC12MEM28 98h
$9A constant ADC12MEM29       \ ADC12_B memory 29 ADC12MEM29 9Ah
$9C constant ADC12MEM30       \ ADC12_B memory 30 ADC12MEM30 9Ch
$9E constant ADC12MEM31       \ ADC12_B memory 31 ADC12MEM31 9Eh





