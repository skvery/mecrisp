\ xmem.fs
\ Extended memory utilities
\ ( for memory map refer to xman.fs )
\ Note that the xx instructions handle 20-bit numbers with non-standard byte sequence. 
\ The xx instuctions use the MSP430 extended instruction set.  
\ The 32-bit number, extended memory instuctions is implemented as 2x instuctions
\ defined here and uses the Mecrisp byte sequence. 
\
\ requires teston.txt or testoff.txt
t{ 1 0 -> 1 0 }t                                  \ confirm inclusion of teston or testoff

: 2and ( d d -- d )                               \ bitwise double and
  rot and -rot and swap inline
; 
t{ 5 3 5 3 2and -> 5 3 }t 
t{ 5 1 3 3 2and -> 1 1 }t

\ ngap space required to align to n-size boundary - required for ringbuffers
: ngap ( caddr n -- n )                           \ number of bytes to the n-boundary
  >r dnegate r>
  0  \ make double 
  1. d- 2and    ( cxaddr xoffset )                \ ( no check for 2^ )
  drop \ make single
; 
t{ align here 0 2 ngap -> 0 }t 
t{ align 1 allot here 0 2 ngap -> 1 }t 

\ Write address register using extended instruction
\ : x! ( d addr -- ) 
\  [ 
\  $4437 ,  
\  $1800 , 
\  $44F7 , 
\  $0000 ,
\  ]
\ ;
t{ align 7 1 here 0 xx! here 0 xx@ -> 65543. }t
t{ align 2 3 here 2! here 0 xx@ -> 3 2 }t
t{ align 4 5 here 0 xx! here 2@ -> 5 4 }t
t{ align 7 $11 here 0 xx! here 0 xx@ -> 7 1 }t  \ NB! only 20-bits saved!
\ Write long address register using extended instruction
\ use this instruction to write xaddr to low memory as required by TI
\ word sequence in MSP430 double register assignment is not the same as Mecrisp
\ write to low memory
: d! ( xadr addr -- )
  0 xx! inline
; 
t{ 8 1 DMAxDA DMA0BASE + d! DMAxDA DMA0BASE + @ 0 DMAxDA DMA0BASE + ! -> 8 }t
\ xpointer write to low memory
t{ align 9 8 here d! here 2@ -> 8 9 }t

: 2x@ ( xaddr -- ud|d )
  2dup 2>r 2. d+
  x@ 2r> x@
  inline
;
t{ $feff $fffe align here 2! here 0 2x@ -> $feff $fffe }t

: 2x! ( ud|d xaddr -- )
  2dup 2. d+ 2>r
  x! 2r> x!
  inline
;
t{ $feff $fffe align here 0 2x! here 2@ -> $feff $fffe }t

: 2x+! ( ud|d xaddr -- )  \ increment extended double memory 
  2dup 2>r 2x@ d+ 2r> 2x! inline
; 
t{ align 6 5 here 0 2x! 2 $fff1 here 0 2x+! here 0 2x@ forgetram -> 8 $fff6 }t

: 2+! ( ud|d addr -- )  \ increment extended double memory 
  dup >r 2@ d+ r> 2! inline
; 
t{ align 6 5 here 2! 2 $fff1 here 2+! here 2@ forgetram -> 8 $fff6 }t

\ extend normal utility functions
: x+! ( u xaddr -- )
  2dup 2>r x@ + 2r> x! inline
; 
t{ compiletoram 7 here 0 x! 2 here 0 x+! here 0 x@ compiletoflash -> 9 }t
: d?dup ( d -- 0. | d d )
  2dup d0= not if 2dup then inline
; 
t{ 0. d?dup ->  0 0 }t  
t{ 1 0 d?dup -> 1 0 1 0 }t 
t{ 0 1 d?dup -> 0 1 0 1 }t
: sz- ( -- ) 2. d-                  \ length of size word also used in xman.fs
; 
t{ 5 5 sz- -> 3 5 }t
: sz+ ( -- ) 2. d+                  \ length of size word also used in xman.fs
; 
t{ 5 5 sz+ -> 7 5 }t
: 2sz- ( -- ) 4. d-                  \ length of xsize word also used in xman.fs
; 
t{ 5 5 2sz- -> 1 5 }t
: 2sz+ ( -- ) 4. d+                  \ length of xsize word also used in xman.fs
; 
t{ 5 5 2sz+ -> 9 5 }t

\ Simple static xmemory allocation define start of xmem
\ keep out 20 bytes
\
\ For memory map see xman.fs
$10000. 2constant HEAP                \ redefined by xman.fs
$44000. 2constant FRAM.TOP              \ 256K MSP430FRxxx4

check

\ $24000. 2constant FRAM.TOP            \ 128K MSP430FRxxx2
\ $1C000. 2constant FRAM.TOP            \ 96K MSP430FRxxx2
\ $14000. 2constant FRAM.TOP            \ 64K MSP430FRxxx2
\
\
check

\ Simple static xmemory allocation define start of xmem
\ For memory map see xman.fs

FRAM.TOP 2constant HEAP.TOP     \ 256K MSP430FRxxx4

HEAP.TOP 2sz- 2variable Next.Handle   \ refer to xman.fs
HEAP          2variable Next.Piece    \ refer to xman.fs
t{ Next.Piece 2@ -> HEAP }t
\ xalloc defined in xman.fs ends all static allocations
: xhere  ( -- xaddr ) 
  Next.Piece 2@
;
: xallot ( size -- ) 
  0  \ make double 
  Next.Piece 2+!
;
: xalign, ( size -- )                             \ align the xmem pointer to the size-boundary for ring buffer 
  Next.Piece 2@ rot
  ngap xallot
;
t{ Next.Piece 2@ $F1F1 1 Next.Piece 2! $100 xalign, Next.Piece 2@ 2swap Next.Piece 2! -> $f200 1 }t 
t{ Next.Piece 2@ -> HEAP }t

\ to initialize xmem use as follows:
\   2 xalign,
\   xhere 2constant (name)
\   1 1 2x,  1 x, 1 x, [char] % cx, 
\   2 xalign, 
\   xhere 2constant (name)
\   2 xbuffer,
\ ?collision is true if no room is available - no automatic checks are performed!
: ?collision ( size -- flag )              \ will xallotting size cause failure?
  0  \ double
  Next.Piece 2@ d+ 2sz+ Next.Handle 2@ d>
; 
t{ Next.Piece 2@ -> HEAP }t
t{ Next.Handle 2@ 8 0 d- Next.Piece 2dup 2>r 2! 5 ?collision 2r> Next.Piece 2! -> true }t
cr 153 . Next.Piece 2@ hex. hex.
HEAP Next.Piece 2!
t{ Next.Handle 2@ 8 0 d- Next.Piece 2dup 2>r 2! 4 ?collision 2r> Next.Piece 2! -> false }t
cr 156 . Next.Piece 2@ hex. hex.
HEAP Next.Piece 2!
t{ Next.Piece 2@ -> HEAP }t
: xbuffer, ( size -- )
  xallot                                          
;
t{ Next.Piece 2@ 2dup 2>r $F1F1 0 d+ $F1F1 xbuffer, Next.Piece 2@ d= 2r> Next.Piece 2! -> true }t
t{ Next.Piece 2@ -> HEAP }t
: x, ( u -- )
  2 xalign,
  Next.Piece 2@ x!
  2 xallot
; t{ 1234 x, -2. Next.Piece 2+! Next.Piece 2@  x@ -> 1234 }t
t{ Next.Piece 2@ -> HEAP }t
: 2x, ( d -- )
  2 xalign,
  next.piece 2@ 2x!
  4 xallot
; t{ 1234 1234 2x, -4. Next.Piece 2+! Next.Piece 2@  2x@ -> 1234 1234 }t
t{ Next.Piece 2@ -> HEAP }t
: cx, ( c -- )
  next.piece 2@ cx!
  1 xallot
; t{ 12 cx, -1. Next.Piece 2+! Next.Piece 2@  cx@ -> 12 }t
t{ HEAP Next.Piece 2! Next.Piece 2@ HEAP d= -> true }t      \ reset Handle
: resetbuffer ( u xaddr -- )   \ usage #name (name) reset counted buffer 
  x!
;

check