del ..\*.hex

cd msp430f1612
c:\aswcurr\bin\asw forth-mecrisp-1612.asm && c:\aswcurr\bin\p2hex forth-mecrisp-1612.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430f2274
c:\aswcurr\bin\asw forth-mecrisp-2274.asm && c:\aswcurr\bin\p2hex forth-mecrisp-2274.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430g2553
c:\aswcurr\bin\asw forth-mecrisp-2553.asm && c:\aswcurr\bin\p2hex forth-mecrisp-2553.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430g2553-lowpower
c:\aswcurr\bin\asw forth-mecrisp-2553-lowpower.asm && c:\aswcurr\bin\p2hex forth-mecrisp-2553-lowpower.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430g2x55
c:\aswcurr\bin\asw forth-mecrisp-2755.asm && c:\aswcurr\bin\p2hex forth-mecrisp-2755.p -r 0x0000-0xffff
c:\aswcurr\bin\asw forth-mecrisp-2855.asm && c:\aswcurr\bin\p2hex forth-mecrisp-2855.p -r 0x0000-0xffff
c:\aswcurr\bin\asw forth-mecrisp-2955.asm && c:\aswcurr\bin\p2hex forth-mecrisp-2955.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430fr2433
c:\aswcurr\bin\asw forth-mecrisp-2433.asm && c:\aswcurr\bin\p2hex forth-mecrisp-2433.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430fr4133
c:\aswcurr\bin\asw forth-mecrisp-4133.asm && c:\aswcurr\bin\p2hex forth-mecrisp-4133.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430f5529
c:\aswcurr\bin\asw forth-mecrisp-5529.asm && c:\aswcurr\bin\p2hex forth-mecrisp-5529.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430fr5969
c:\aswcurr\bin\asw forth-mecrisp-5969.asm && c:\aswcurr\bin\p2hex forth-mecrisp-5969.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430fr5994
c:\aswcurr\bin\asw forth-mecrisp-5994.asm && c:\aswcurr\bin\p2hex forth-mecrisp-5994.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..

cd msp430fr6989
c:\aswcurr\bin\asw forth-mecrisp-6989.asm && c:\aswcurr\bin\p2hex forth-mecrisp-6989.p -r 0x0000-0xffff
 move /Y *.hex ..\..\
del *.p
cd ..
