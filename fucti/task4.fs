\ task4.fs

0 variable SPfree

: loopSP ( -- )
  begin
    SPfree wait
    prepareSP
    saveSP
  again
;

task: task4                      \ this task creates SP and copies SP to profile memory 
: task4& task4 activate
  loopSP  
;
: inittask4
  task4& multitask
;

: init init
  inittask4
;

