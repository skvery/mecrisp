\ wait.fs
\
\ various wait routines that can be updated later to interrupt coroutines if needed.

: waitflagset? ( mask register -- mask register flag )
   pause 2dup bit@
;

: waitflagreset ( mask register -- )  \ pause until masked flag in register is false
  begin waitflagset? not until
  2drop
;

: waitflagset ( mask register -- )  \ pause until masked flag in register is true
  begin waitflagset? until
  2drop
;

: milliflagset ( mask register milli -- delta )  \ wait for u milliseconds for flag to set and return remainder 
  milli? + 0 ( mask register oldmilli 0 -- ) 
  begin 
    pause
    2swap                          ( oldmilli 0 mask register -- )
    2tuck bit@ swap drop           ( mask register oldmilli flag -- )
    over pause milli? - tuck 0< or ( mask register oldmilli delta flag -- )
  until
  2swap 2drop swap drop
  dup 0< if drop 0 then
;

: cwaitflagset? ( mask cregister -- mask cregister flag )
  pause 2dup cbit@
;
: cwaitflagset ( mask cregister -- )  \ pause until masked flag in register is true
  begin cwaitflagset? until
  2drop
;

: cwaitflagreset ( mask cregister -- )  \ pause until masked flag in register is true
  begin cwaitflagset? not until
  2drop
;

: waitmilli? ( until -- until flag )
  pause dup milli? - 0<
;

: waitmilli ( u -- )  \ wait for u milliseconds ( not restart safe )
  milli? + 
  begin waitmilli? until
  drop
;

\ : uscxFree ( eUSCIxBase -- ) \ call pause until USCx is free (for baud rate change etc) 
\   STATW +
\   begin
\     pause
\     .0
\     over
\     cbit@
\   until
\   drop
\ ;

: waitsec ( u -- ) \ wait for u seconds ( not restart safe )
  sec? +
  begin pause dup sec? - 0< until
  drop
;

: 2waitsec ( ud -- ) \ wait for ud seconds ( not restart safe )
  2sec? d+
  begin pause 2dup 2sec? d- d0< until
  2drop
;

\ maybe wait for powerup belongs here... maybe not?