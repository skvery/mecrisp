\ saveSP.fs
\
\ This code:
\ - checks if the sample time is lapsed;
\ - calculates the sample checksum
\ copies the sample to profile memory
\
\ xhere 2constant P0.S   100 x,          \ 2(2)+  interval +sampling or recording interval in seconds ( 900 )
\ xhere 2constant P0.T     0 x,          \ 3(2)+  offset   +positive offset from epoch to real-timepoch sample time
\ xhere 2constant P0.I0    0 x,          \ 4(2)   index0    pointer to start of oldest record
\ xhere 2constant P0.I1    0 x,          \ 5(2)   index1    pointer to start next record to be written
: copySP ( -- )
  begin
    P0.I0 x@ P0.I1 x@ - P1.L x@ - 0<             \ while no space in the profile buffer for this record?
  while
    P0.I0 x@ dup 1 x@ + P0.I0 x!                 \ try to make room by incrementing oldest record ( profile at **** 1 )
  repeat
  ( over here we still need to handle the 64K boundary )
  P1.L x@ 0  P0.I1 x@ 1                          \ ( dlength daddr )
  d+
  2 = if
    >r
    P1.L x@ r@ - 0
    P2.I2 cx@ 1+ (Q)i dx@                        \ ( dlength saddr )
    P0.I1 x@ 1                                   \ ( profile at **** 1 )
    verbose @ 2 = if cr ." 2.1=" .s then
    copy16
    r@ 0 P1.L x@ r@ - 0 P2.I2 cx@ 1+ (Q)i dx@ d+ \ ( dlength saddr )
    0 1                                          \ ( profile at **** 1 )
    verbose @ 2 = if cr ." 2.0=" .s then
    copy16
    verbose @ 2 = if cr ." 3.0=" .s then
    dma5free dup wait signal                     \ wait for completion before updating pointers 
    r> P0.I1 x!
  else
    drop
    verbose @ 2 = if cr ." 1=" .s then
    P1.L x@ 0 P2.I2 cx@ 1+ (Q)i dx@              \ ( dlength saddr )
    P0.I1 x@ 1                                   \ ( profile at **** 1 )
    verbose @ 2 = if cr ." 2=" .s then
    copy16
    verbose @ 2 = if cr ." 3=" .s then
    dma5free dup wait signal                     \ wait for completion before updating pointers 
    verbose @ 2 = if cr ." 4=" .s then
    P0.I1 x@ dup 1 x@ + P0.I1 x!                 \ advance pointer to start next record to be written
    verbose @ 2 = if cr ." 5=" .s then
  then
  verbose @ 2 = if cr ." Saved to profile memory!" then
;
\ xhere 2constant (P1)
\ xhere 2constant P1.L      2. xbuffer,          \ 10(2)      length      total number of bytes including length and checksum
\ xhere 2constant EPOCH0-HH 2. xbuffer,   
\ xhere 2constant EPOCH0-LL 2. xbuffer,          \ 11(4)      epoch       per second running timer that cannot be adjusted
\ xhere 2constant EPOCH-CC  2. xbuffer,          \ 12(2)      cepoch      time check only valid if -LL = -CC
: saveSP ( -- )
  verbose @ 2 = if cr ." -3.0=" .s then
  P2.I2 cx@ 1+ (Q)i dx@                          \ end of response area and start of scratchpad
  (P1) d- 2dup 2>r
  EPOCH0-LL d+ x@                                \ read EPOCH0-LL from scratchpad
  2r>  
  EPOCH-CC d+ x@                                 \ read EPOCH0-CC from scratchpad
  verbose @ 2 = if cr ." -3.1=" .s then
  = if                                           \ EPOCH valid ?
    P2.I2 cx@ 1+ (Q)i dx@                        \ end of response area and start of scratchpad
    (P1) d- EPOCH0-HH d+ dx@                     \ read EPOCH0 from scratchpad
    P0.T x@ 0 d+ P0.S x@ um/mod
	swap drop                    \ next sample id (adding offset)
	dup
	P2.S# x@                                     \ current sample id
    verbose @ 2 = if cr ." -2=" .s then
    <> if
      \ if verbose dup . then                      \ not equal?
      P2.S# x!                                   \ update sample id
      P2.I2 cx@ 1+ (Q)i dx@                      \ end of response area and start of scratchpad ( saddr )
      verbose @ 2 = if cr ." -2.1=" .s then
      P1.L x@ 2- 0 2dup 2>r 2over                \ ( saddr dlength-2 saddr r:dlength-2 ) 
      verbose @ 2 = if cr ." -2.2=" .s then
      crc16 -rot 2r> d+ x!                       \ save CRC
      copySP
	  verbose @ 2 = if cr ." -2.3=" .s then
    else
      verbose @ 2 = if cr ." 0=" .s then
      drop
      verbose @ 2 = if cr ." 0=" .s then
      
    then 
  then
;

\ start checksum calculation
\ find profile head and tail
\ find last save
\ wait for checksum complete
\ transfer to profile
\ update profile head and tail
\ 
