\ lpm35.fs
\
\ Low Power Mode 3.5 Test with RTC
\ ( Wake up once per second )
\ run3 use pause
\ run4 use lpm4
\ run5 use lpm5
\ -------------------------------
eraseflash
compiletoflash
1 constant .0
$0 constant PxIN
$2 constant PxOUT
$4 constant PxDIR
$6 constant PxREN
$A constant PxSEL0
$C constant PxSEL1
$0200 constant P0Base
$0320 constant PJBase
$04A0 constant RTCBase
$A5 constant RTCKEY
$01 constant RTCLOCK
$02 constant RTCCTL13
$0A constant RTCPS1CTL_L
$0E constant RTCIV
$0C constant RT1PSIFG
$10 constant PMM5CTL0
$00 constant PMMCTL
$01 constant PMMCTL_H
$0120 constant PMMBase
$015C constant WDTBase
$00 constant WDTCTL
$019E constant SYSRSTIV
$08 constant PMMLPM5

$D3FE constant BACKDOOR
: toggle ( -- )
  $10000. xx@ 1. d+ $10000. xx!
  .0 PxOUT P0Base + cxor!        \ P0OUT LED1 for diagnostic
  .0 PxDIR P0Base + cbis!        \ P0DIR LED1 for diagnostic
;
: rtcisr ( -- )
  RTCIV RTCBase + @ drop         \ read from RTCIV clears interrupt
  wakeup
;
: rtc ( -- )           \ 32.768 kHz with calender mode
      \ all ports need setup here for minimum consumption
      0 PxDIR    PJBase  + c!     \ PxDIR PJ_Base - set port minimum consumption
  $FFFF PxREN    PJBase  + c!     \ PxREN PJ_Base - input with pull
      0 PxSEL1   PJBase  + c!     \ PJ_Base PxSEL1 - lfxt
    $30 PxSEL0   PJBase  + c!     \ PJ_Base PxSEL0 - lfxt
 RTCKEY RTCLOCK  RTCBase + c!     \ RTCKEY   RTCCTL0_H RTC_BASE - unlock
  $0020 RTCCTL13 RTCBase + !      \ RTCMODE  RTCCTL13 RTC_BASE - calender
    $FF RTCLOCK  RTCBase + c!     \ FF       RTCCTL0_H RTC_BASE - lock
 $1A RTCPS1CTL_L RTCBase + c!     \ RTC/128 IE RTCPS1CTL_L RTC_BASE - enable
     .0 PMM5CTL0 PMMBase + cbic!  \ LOCKLPM5 PMM5CTL0 PMM_BASE - unlock ports
    $A510 PMMCTL PMMBase + !      \ PMMPWD PMMREGOFF PMMCTL0      - sel lpm5
    $FF PMMCTL_H PMMBase + c!     \ FF               PMMCTL0_H - lock
      0 RTCIV    RTCBase + !      \ RTCIV - clear
;
: lpm5 ( -- na )          \ lpm5 power off
  [ $D072 , $00F0 , ]     \ BIS.B #CPUOFF+OSCOFF+SCG0+SCG1, SR
; 
: wakeup5 ( -- )  \ init placeholder for cold startup configuration
  \ use MODOSC
  \ $A5 1 $160 + c!              \ KEY CSCTL0_H \ unlock CS
  \ $24 4 $160 + c!              \ SELS2 SELM4 CSCTL2_L \ SMCLK-LFMODCLK MCLK-MODCLK
  \ $FF 1 $160 + c!              \ LOCK CSCTL0_H
  \ if pmmlpm5ifg set
  PMMLPM5 SYSRSTIV @ =           \ SYSRSTIV = PMMLPM5
  if
    rtc
    toggle \ LED not on
    lpm5
  then
  $FFFF BACKDOOR flash!
  reset
;
\ ************************************************
: run3                    \ use pause to toggle LED
  rtc
  begin
    begin 
      pause 
      .0 RTCPS1CTL_L RTCBase + cbit@ \ IFG RTCPS1CTL_L RTC_BASE - flag
    until   \ RTCTEVIFG
    0 RTCIV RTCBase + !            \ RTCIV - clear
    toggle
  again
;
: run4
  rtc
  ['] rtcisr irq-rtc !    \ # irq reset and wakeup!
  eint
  begin
    lpm2                  \ power save until interrupt continue
    toggle
  again
;
: run5
  cr ." Fram count reset "
  cr
  ['] wakeup5 BACKDOOR flash!    \ lpm5 reset and wakeup!
  0. $10000. xx!                 \ clear counter
  rtc
  lpm5                    \ CPU core power down
;
: init
  ." Fram count now at: "  $10000. xx@ d. cr
;
