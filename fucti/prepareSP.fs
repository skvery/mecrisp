\ prepareSP.fs
\
\ default scratchpad calculator
\ this code must chain writeSP to ensure that the the profile is written if the sample time has passed.
\ ( the checksum calculation is performed by save before calling the actual writeSP routine )
\
\ start of (P1)
\ 2. xalign,
\ xhere 2constant (P1)                                          \ start of response area
\ xhere 2constant P1.L      2. xbuffer,  \ 10(2)      length      total number of payload bytes including checksum
\ xhere 2constant EPOCH0-HH 2. xbuffer,  \ 
\ xhere 2constant EPOCH0-LL 2. xbuffer,  \ 11(4)      epoch       per second running timer that cannot be adjusted
\ xhere 2constant EPOCH-CC  2. xbuffer,  \ 12(2)      cepoch      time check only valid if -LL = -CC
\ xhere 2constant (P2)
\ xhere 2constant P2.S#      2. xbuffer, \ 21(2)      id          next profile sample number
\ xhere 2constant P2.I2      2 cx,       \ 22(1)      index2      (+) number of values in query and profile table, 0-15
\ xhere 2constant P2.#       3. xbuffer, \ 23(3)      seed        random hex number in silicon
\ xhere 2constant P2.S       0 x,        \ 24(2)      status      7654nnnnFEDCBA98 last nibble is number power downs
\ xhere 2constant (R)
\ xhere 2constant (R0) 25 x, 25. xbuffer,                       \ #R0 is defined as part of (Q)0  

: prepareSP ( -- )
  P2.I2 cx@ 1+ (Q)i dx@                \ end of response area
  2dup 2>r
  (P1)                                 \ start of respomse area
  2dup 2>r
  d-                                   \ calculate the length
  2dup drop 2+                         \ make single ( < 64 K ) and make space for checksum
  P1.L x!                              \ save the length
  2r>
  2r>
  copy16  
;
check
\ saveSP
check