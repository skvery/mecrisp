\ task310.fs
\  +---------+
\  |state3.10| on /?! message already saved to scratchpad
\  |         |
\  +----+----+
\       |
\       | record serial
\       | (scratchpad)
\       |       +------+
\       |       |      |
\ +-----v-------v-+    | record response
\ |3.101,3.102... |    |
\ | for each...   +----+
\ +---------------+
\ |read table     |
\ |send command   |
\ |record response|
\ |next...        |
\ +-----+---------+
\       |
\       +------------------------> add checksum ( >profile ) to 3.100
\
: case3.10
  q# @ 1+ dup P2.i2 cx@ > if drop 0 then
  dup q# ! 
  tx3rx3
  0 = if -1 err# +! else maxerr err# ! then
  err# @ 0 = if 4 state3 ! then
;
task: task3                      \ this is the local task 
: task3& task3 activate
  begin
    SPfree signal 
    2000 waitmilli 
    state3 @
    case
      2 of case3.2 endof
      4 of case3.4 endof
      6 of case3.6 endof
      8 of case3.8 endof
      10 of case3.10 endof
      12 of case3.12 endof
      case3.*
    endcase
  again
;
: diag
  ." state   " state3 @ . cr
  ." err#    " err# @ . cr
  ." q#      " q# @ . cr
  ." rxterm# " rxterm# @ . cr
  ." rxterm? " rxterm? @ . cr 
  P2.i2 cx@ 2+ 0 do i dup . (Q)i dx@ showx loop
;
: init init
  0 state3 ! 
  task3&
;
check