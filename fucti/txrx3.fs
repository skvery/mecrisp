\ txrx3.fs
\ \\\\\\\\\\\\\\\\\\\\\\\\
\ Tx and Rx utilities for uca3
\
\ These utilities are used by the task 3 state machines to use the query table and to write the response table.
\
\ The main functionality is the termination of the rx function. The termination conditions are as follows:
\ 
\ - time-out
\ - complete rx buffer received
\ - final termination character received - conditional for additional check characters
\
\ Note that this is a working first draft and cleanup is required.
: c2
  uca3config300
  cdmauca3tx
  cdmauca3rx
;
: c4
  uca3config2400
  cdmauca3tx
  cdmauca3rx
;
: c6
  uca3config9600
  cdmauca3tx
  cdmauca3rx
;
: tx3 ( Array -- )
  UCSWRST                  eUSCI-A3-Base CTLW0 + bic! \  stop usci
  pause
  uca3tx
  UCTXIFG    IFG eUSCI-A3-Base + waitflagset          \ wait txbuf
  UCTXCPTIFG IFG eUSCI-A3-Base + bic!                 \ clear stop bit flag   
  UCTXCPTIFG IFG eUSCI-A3-Base + waitflagset          \ wait stop bit flag if we miss use time-out
;
: doneDMA? ( endmilli char size -- endmilli char size flag )
    drop              ( endmilli char )  
    pause
    0 (R0) x!
    #R0 DMAIFG DMAxCTL DMA3-Base + bit@                 \ buffer full - DMA flag set? 
;
: rx2termchar? ( endmilli char size -- endmilli char size flag )
  pause
  DMAxSz DMA3-Base + @ - 2dup 1+ 0 (R0) d+ cx@ = \ rx = char? NBNB!
;
: rx2time? ( endmilli char size -- endmilli char 0 flag )
  pause  
  drop 0 2 pick milli? - 0<                                   \ 0 endmilli?
;
: rx3checkend ( char milli -- size )  \ wait for u milliseconds and return code
  milli? + swap 0 ( endmilli char size ) 
  begin
    doneDMA?
    if                ( endmilli char #R3Buffer )
      true
    else
    rx2termchar?
    if                ( endmilli char size )
      true
    else
    rx2time?
    if                ( endmilli char 0 )
      true
    else
      false
    then then then
  until
  DMAEN DMAxCTL DMA3-Base + bic!                                 \ stop DMA if not already stopped 
  -rot 2drop ( size )
;
: rx3 ( char milli Array -- size )
  2tuck                    ( Array char milli Array )
  UCSWRST                  eUSCI-A3-Base CTLW0 + bis! \  stop usci
  dmauca3rx                ( Array char milli )
  UCSWRST                  eUSCI-A3-Base CTLW0 + bic! \  start usci 
  rx3checkend              ( Array size )
  dup 2swap
  x!
;
0 variable state3
3 constant maxerr
0 variable err#         \ error count
0 variable q#           \ query counter
2000 constant waitforrx
0 variable rxterm#
0 variable rxterm?
: waitabit
  1 waitmilli
;
: tx2rx2 ( rsize raddr qaddr -- result )
  tx3
  waitabit               \ wait only a bit
  2dup 2>r resetbuffer
  -LF 2000 2r> rx3
;
: (Q)clr ( i -- )
  dup 1+ (Q)i dx@ drop swap (Q)i dx@ -rot do dup $FFFF swap i swap x! 2 +loop drop
;  
: (Q)itchar ( i -- i tchar )
  dup (Q)i 6 0 d+ x@ $FF and
;
: (Q)itl ( i -- i tl )
  dup (Q)i 6 0 d+ x@ 8 rshift $7F and
;
: doneQiDMA? ( i progress -- i progress/maxresponse flag )        \ DMA flag means budder is full!
  DMAIFG DMAxCTL DMA3-Base + bit@                 \ buffer full - DMA flag set?
  dup if 
    swap drop over (Q)i 4 0 d+ x@ swap
  then
;
: rxQitime? ( oldmilli i progress -- oldmilli i progress flag)
  2 pick milli? - 0<                                   \ 0 endmilli?
;
: rxQitermchar ( i progress -- i progress )  \ scan up to DMAxSz for first termchar
  over (Q)i 4 0 d+ x@ DMAxSz DMA3-Base + @ - 
  over  ( i progress size progress )                                          
  ?do
    drop                                          ( i )
    pause
    rxterm? @                                    \ termchar done
    if
      rxterm# @ 0 <>                             
      if
        -1 rxterm# +! 
      then
    else                                          
      (Q)itchar                                   ( i c )
      over (Q)i dx@ 2 i + 0 d+ cx@                ( i c c )
      =
      if
        true rxterm? !
        (Q)itl rxterm# !                         \ scan rxterm# more
      then
    then
    i                                            \ progress back
    1+
  loop
;
: rx2checkend ( i -- size )  \  return no of received chars
  waitforrx milli? + swap 0 ( oldmilli i 0 ) 
  0 rxterm# !
  0 rxterm? !
  begin
    doneQiDMA?
    if                ( oldmilli i progress )
      true 
    else
    rx2time?
    if                ( oldmilli i progress )
      true 
    else
      rxQitermchar   ( oldmilli i progress )          \ scan input for char 
      rxterm? @ rxterm# @ 0 = and                
    then then
  until
  DMAEN DMAxCTL DMA3-Base + bic!                      \ stop DMA if not already stopped
  -rot 2drop ( size )
;
: rx2 ( i -- size )
  .2                       PxOUT P6_Base + bis!       \ P6.2 RxEn high
  UCSWRST                  eUSCI-A3-Base CTLW0 + bis! \  stop usci
  0 over (Q)i dx@ 2dup     dmauca3rx                  ( i 0 xaddr )
  UCSWRST                  eUSCI-A3-Base CTLW0 + bic! \  start usci 
  x!                                                  \ save 0 to (S3) length
  dup rx2checkend              
  .2                       PxOUT P6_Base + bis!       \ P6.2 RxEn low
  dup -rot swap (Q)i dx@ x!                           \ save actual response length
;
: tx3rx3 ( i -- result )    \ uses query number
  dup (Q)i 8 0 d+ tx3
  waitabit                  \ wait only a bit
  dup reset(R)buffer
  rx2
  verbose @ if dup u. then 
;
