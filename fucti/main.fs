\ main.fs
\
\ Forth UCT Project
\ 
\ main file
\
\ -------------------------------
\

eraseflash
compiletoflash
  
include setupdebug.fs
include tester.txt
\ include testerno.txt

: init ( -- )  \ init placeholder for cold startup configuration
   cr ." ...head of init"
;

\ constants
include setconstants.fs

\ io utilities
include setuputility.fs

\ extended memory utilities
include xmem.fs

\ see simple allocation for allocated extended memory

include setupX0000h.fs

include setuprtc.fs

include setupepoch.fs
include setupmilli.fs
include wait.fs
include setupserial.fs

include multitask.txt

include setupdma.fs
include saveSP.fs
include prepareSP.fs
include task1.fs
include terminal.fs

include setuptemp.fs

\ all module init code prepared
\ start preparing main modules

include task4.fs
include txrx3.fs
include task3.fs
include task310.fs

: run 
  task3&
;
\ temp stuff
: clearprofile ( -- )
  16K 0 1 x!                   \ four 16K dummy blocks tn profile
  16K 16K 1 x!
  16K 32K 1 x!
  16K 16K 32K + 1 x!
  0 P0.I1 x!                   \ pointer to start of next record 
  0 P0.I0 x!                   \ pointer to start of of oldest record        
;
\ : debuginit
\ ;

: init  
  init cr ." ...tail of init" 
;
\ init
\ run
check
