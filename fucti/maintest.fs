\ main.fs
\
\ Forth UCT Project
\ 
\ main file
\
\ -------------------------------
\

eraseflash reset
compiletoflash
  
\ debugging
\ include setupdebug.fs
include tester.fs
\ include notester.fs

: init ( -- )  \ init placeholder for cold startup configuration
   cr ." ...head of init"
;

\ constants
include setconstants.fs

\ io utilities
include setuputility.fs

\ extended memory utilities
\ include xmem.fs

\ see simple allocation for allocated extended memory

\ include setupX0000h.fs
include fail.fs

include setuprtc.fs

: epoch-isr ( -- )              \ serves epoch counter in fail-safe way - low power mode?                           
  1 Epoch0-LL x@ + Epoch0-LL x! \ increment double Epoch0-HH:Epoch0-LL
  Epoch0-LL x@ Epoch-CC x!   \ copy Epoch0-LL to EpochCC
  0 RTC_BASE RTCIV + !       \ write interrupt vector to reset 
  \ eint                     \ it is now safe to use Epoch0
  toggle2                    \ from utility toggle green LED 
;
: initepoch
   ['] epoch-isr irq-rtc !
\   eint
;
: init init
\  initepoch
;

\ include setupepoch.fs
\ include setupmilli.fs
\ include wait.fs
\ include setupserial.fs
\ include setupdma.fs
\ include terminal.fs

\ include setupmultitask.fs
\ include setuptemp.fs

\ all module init code prepared
\ start preparing main modules

\ include txrx3.fs
\ include task3.fs
\ : run 
\  task3&
\ ;
\ include task310.fs

\ temp stuff

\ check

\ : debuginit
\ ;

: init  
  init cr ." ...tail of init" 
;
\ init
\ debuginit
\ run

\ check