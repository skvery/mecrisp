\ task1.fs

\ tic-toc multitask heartbeat
: toc ( -- )
  sec?
  begin
    pause
    dup sec?
    <> if 
      drop sec? toggle1
    then
  again
;

task: task1                      \ this is a dummy heartbeat task - see multitask-examples.txt 

: task1& task1 activate
  toc  
;
: inittask1
  task1& multitask
;

: init init
  inittask1
;