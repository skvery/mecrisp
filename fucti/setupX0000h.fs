\ setupX0000h.fs
\ 
\ see setupx.fs line 7 for definition of actual start point of static external memory
\
\ page 0 lower 64 K
\ page 1 fram 64 K reserved as profile memory
\ page 2, onwards, fram to be used for buffers
\ (Remember to arrange allotments from large to small!)
\
\ reserve page one (and two)
32K xalign,
32K xallot
32K xallot
t{ next.piece 2@ -> 0 2 }t
0 x,
1 x,
2 x,
3 x,
4 x,
5 x,
6 x,
7 x,
8 x,
9 x,
$A x,
$B x,
$C x,
$D x,
$E x,
$F x,
\ reserve forth console circular Rx buffer
$100 ( 1K ) xalign, xhere 2constant (R0BUFFER)
$100 ( 1K ) xbuffer,
$100 ( 1K ) constant #R0BUFFER                  \ buffer size for Rx
\ Query table is fixed length while the response table is variable size to cater for larger or
\ variable response files.
\ the pointers and sizes of the response table are included in the query table
\ the length field determines the maximum number of bytes in the message
\ Queries table q#length with:
\         (R)pointer, maxresponse, type, length, query, garbles  ( checksum )
\ offsets:     0,          4,       6,     8,      10,    ....   ( q#length-2 )
$100 constant Q#LENGTH                 \ maximum query length of 256 bytes
Q#LENGTH 0 xalign, xhere 2constant (Q)
4 xbuffer,  25 x, $A x,               \ termination character $A {lf}
                       25 constant #R0 \ #R0 maxlength fixed for (Q)0 only
  xhere 2constant (Q0)                 \ (Q0) $0005 2f 3f 21 0d 0a  {$0005}"/?! {cr}{lf}"
          5 x,    
  [char] / cx, 
  [char] ? cx, 
  [char] ! cx, 
  -CR cx, 
  -LF cx,
q#length 0 xalign,
4 xbuffer, 30 x, $103 x,              \ termination character $3 plus one more checksum character
  xhere 2constant (Q1)                 \ (Q1Array) soh ... "{soh}R{stx}4002 7{etx}{bcc}" 
         10 x,                         \ (Q13Array) soh ... "{soh}R{stx}2006 6{etx}{bcc}" 
      -SOH cx,                         \ excluded from xor bcc
  [char] R cx, 
      -STX cx, 
  [char] 2 cx, 
  [char] 0 cx, 
  [char] 0 cx, 
  [char] 6 cx, 
  [char] 6 cx, 
      -ETX cx, 
        97 cx,                         \ included ito bcc BITXOR from google sheet!
q#length 0 xalign,
4 xbuffer, 128 x, $103 x,
  xhere 2constant (Q2)                 
         10 x,                         \ (Q2) soh ... "{soh}R{stx}0802 0{etx}{bcc}" 
      -SOH cx,                         \ excluded from xor bcc
  [char] R cx, 
      -STX cx, 
  [char] 0 cx, 
  [char] 8 cx, 
  [char] 0 cx, 
  [char] 2 cx, 
  [char] 0 cx, 
      -ETX cx, 
  [char] i cx,                         \ included ito bcc BITXOR from google sheet!
q#length 0 xalign, 
q#length 13 * 0 xbuffer,
2 xalign,  
xhere 2constant EPOCH1-HH 2. xbuffer, 
xhere 2constant EPOCH1-LL 2. xbuffer, 
\ (P)
2 xalign, 
xhere 2constant (P0) 
xhere 2constant P0.L     0 x,          \ 0(2)   preamble length including the length and checksum
xhere 2constant (QR)                   \ 1(1030)+ qu-re +query-response field
xhere 2constant (QR).MR  0 x,          \        maxresponse      
xhere 2constant (QR).T   0 x,          \        type
xhere 2constant (QR).QL  0 x,          \        length 
xhere 2constant (QR).Q 1024 xbuffer,  \        query
xhere 2constant P0.S     2 x, ( 900 )  \ 2(2)+  interval +sampling or recording interval in seconds
xhere 2constant P0.T     0 x,          \ 3(2)+  offset   +positive offset from epoch to real-timepoch sample time
xhere 2constant P0.I0  32K x,          \ 4(2)   index0    pointer to start of oldest record
xhere 2constant P0.I1    0 x,          \ 5(2)   index1    pointer to start next record to be written
xhere 2constant P0.6     0 x,          \ 6(2)   internal  voltage 
xhere 2constant P0.7     0 x,          \ 7(2)   internal  temperature
xhere 2constant P0.8     0 x,          \ 8(2)   field8
xhere 2constant P0.9     0 x,          \ 9(2)   field9
xhere 2constant P0.A     0 x,          \ A(2)   fieldA
xhere 2constant P0.B     0 x,          \ B(2)   fieldB
xhere 2constant P0.C     0 x,          \ C(2)   fieldC
xhere 2constant P0.D     0 x,          \ D(2)   fieldD
xhere 2constant P0.E     0 x,          \ E(2)   fieldE
2 xalign,
xhere 2constant P0.CS 2 xbuffer,      \ F(2)   checksum
\ ----fields from here are copied by the default prepareSP, to the scratchpad memory---
\ start of (P1)
2 xalign,
xhere 2constant (P1)
xhere 2constant P1.L      2 xbuffer,  \ 10(2)      length      total number of bytes including length and checksum
xhere 2constant EPOCH0-HH 2 xbuffer,  \ 
xhere 2constant EPOCH0-LL 2 xbuffer,  \ 11(4)      epoch       per second running timer that cannot be adjusted
xhere 2constant EPOCH-CC  2 xbuffer,  \ 12(2)      cepoch      time check only valid if -LL = -CC
xhere 2constant (P2)
xhere 2constant P2.S#      2 xbuffer, \ 21(2)      id          next profile sample number
xhere 2constant P2.I2      2 cx,       \ 22(1)      index2      (+) number of values in query and profile table, 0-15
xhere 2constant P2.#       3 xbuffer, \ 23(3)      seed        random hex number in silicon
xhere 2constant P2.S       0 x,        \ 24(2)      status      7654nnnnFEDCBA98 last nibble is number power downs
xhere 2constant (R)
xhere 2constant (R0) 25 x, 25 xbuffer,                       \ #R0 is defined as part of (Q)0  
2. xalign,
\                                      \ 30(26) maxlength for 1107 exactly as returned by /?!
xhere 2constant (R1) 24 x, 24 xbuffer, 24 constant #R1
2 xalign,                             \ 31(24)  ...
xhere 2constant (R2) 128 x, 128 xbuffer, 128 constant #R2
2 xalign,                             \ 32(...)  ...
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
\  NOTE THAT THIS BUFFER GROWS TO OVERWRITE ANYTHING DEFINED LATER
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

\ save lengths required
: (Q)i \ ( i -- (Q)i )                 \ external address of (Q)i
  Q#LENGTH um* (Q) d+                  \ all records length Q#LENGTH
;
: 2align ( daddrLH - daddrLH )         \ even byte alignment
   over 1 and 0 d+
;
: Qoffset+! ( i -- )                   \ calculate the (Q)i+1 pointer using Q(i)
     (Q)i 2dup 2x@                     \ ( (Q)i (R)i* )
     2over 4. d+ x@ 2+ 0 d+            \ unaligned!  ( (Q)i ((R)*+maxresponse)i )
     2align                            \ 2aligned
     2swap Q#LENGTH 0 d+ 2x!           \ save i+1 pointer
;
: reset(R)buffer ( i -- )              \ reset response buffer i to maxresponse
  (Q)i 2dup 2x@ 2>r 4. d+ x@ 2r> x! 
;
: Qoffsetscalc! ( -- )                 \ calculate save the (Ri) pointers
  (R0) (Q) 2x!
  P2.i2 cx@ 1+ 0 do
    i Qoffset+!
    i reset(R)buffer
  loop 
;
\ write random#s
$1A30 c@ P2.# cx!       \ constant RANDOM0
$1A31 c@ P2.# 1. d+ cx! \ constant RANDOM1
$1A32 c@ P2.# 2. d+ cx! \ constant RANDOM2
\ Note that crc16 routines are in setupdma.fs
\
\ Semaphores
0 variable Plock \ Lock for PBuffer ( unlocked by RX )
\ 1 variable Slock \ SP lock
\ 1 variable Tlock \ $10000 lock
: init init
  Qoffsetscalc!
;
check