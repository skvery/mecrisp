\ State Diagram + Task3   (http://asciiflow.com)
 \
 \      +----+
 \      |(0) |
 \      |init|
 \      +----+
 \         |
 \         <------------------------------------------------+
 \         |                                                |
 \ +-------v------+    +--------------+    +--------------+ |
 \ |     ( 2 )    |    |    ( 4 )     |    |    ( 6 )     | |
 \ |  try 300 bd  +----> try 2400 bd  +----> try 9600 bd  +->
 \ |              |    |              |    |              | |
 \ +--------------+    +--------------+    +--------------+ |
 \         |              |   |                  |          |
 \ +----------------------+   |       +----------+          |
 \ |       |                  |       |                     |
 \ |       |         +--------v-------v-+                   |
 \ |       |         |(8) optional      |                   |
 \ |       +-------->| baud rate change |                   |
 \ |                 +------------------+                   |
 \ |       +-----+            |                             |
 \ |       |     |            |                             |
 \ |  poll |   +-v------------v-----------------+           |
 \ |       |   |( $13 )  main task 3 loop       |           |
 \ |       +---+                                |           |
 \ |           |( read command line from table )|  error    |
 \ |           |( send command to local de^ice )+----------->
 \ |           |( read and save to scratchpad  )|           |
 \ |           |( transfer scratchpad          )|           |
 \ |           |( re-check query table crc     )|           |
 \ |           |( repeat                       )|           |
 \ |           |                                |           |
 \ |           +--------------------------------+           |
 \ |       +-----+                                          |
 \ |       |     |                                          |
 \ |  poll |   +-v------------------------------+           |
 \ |       |   |( $A ) prepay task 3 loop       |           |
 \ |       +---+                                |           |
 \ |           |( read command line from table )|  error    |
 \ |  M($A)    |( send command to local device )+-----------^
 \ +----------->( read and save to scratchpad  )|
 \             |( transfer scratchpad          )|
 \             |( re-check query table crc     )|
 \             |( repeat                       )|
 \             |                                |
 \             +--------------------------------+

\ Task handler for communication to local device over IEC 1107
\
\ The initial states try to establish communication at different baud rates.  Once the device serial number is
\ identified the main loop will ececute a periodic read of the meter, once every few seconds.
\
\ The fields read will be written to a scrachpad area.  The fields will be transferred to the profiler
\ as per configured times.
\ 
\ If errors occur the initial state will be selected.
: case3.2
  c2
  pause
  #R0 (R0) (Q0) tx2rx2
  0= if 4 else 8 then state3 ! 
;
: case3.4
  c4
  pause
  #R0 (R0) (Q0) tx2rx2
  case
     10 of (R0) 3. d+ cx@ -M = if maxerr err# ! 1 q# ! 10 else 6 then endof
      0 of 6 endof
      8 of 8 endof
  endcase
  state3 ! 
;
: case3.6
  c6
  pause
  #R0 (R0) (Q0) tx2rx2
  0= if 2 else 8 then
  state3 !
;
: case3.8
  pause
  2 state3 !
;
: case3.12
  pause
;
: case3.*
  pause
  4 state3 !
;
