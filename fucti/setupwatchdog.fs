\ setupwatchdog.fs

$015C constant WDTCTL                   \ watchdog timer control word register

$5A00     constant PASSWD               \ watchdog register password
%00111100 constant WDCTL_L              \ binary[] wdthold (aclk ACLK) WDTTMSEL WDTCNTCL (WDTIS wdtis wdtis) = 1s  

\ enable interrupts
$0100 constant SFRIE1

.0 SFRIE1 bis!


PASSWD WDCTL_L or  WDTCTL !             \ configure and start wdt

