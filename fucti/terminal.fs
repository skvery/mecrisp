\ terminal.fs
\
\ currently running 2K buffer
\ init to reset to default values
\
\       hook-emit?      ( -- a-addr ) Hooks for redirecting
\       hook-key?       ( -- a-addr )   terminal IO
\       hook-key        ( -- a-addr )     on the fly
\       hook-emit       ( -- a-addr )

#R0Buffer 1 - constant R0Mask  \ helper
0 variable key#

: term-key? ( -- flag )
  pause
  key# @ DMAxSZ  DMA0-Base + @ + R0Mask and \ 0<> this is not needed.
;

: term-key ( -- char )
  begin term-key? until
  (R0Buffer) key# @ R0Mask and 0 d+ cx@ 1 key# +!
; 

: initrx                    \ configure terminal rx buffer
  uca0config115200
  cdmauca0rx 
  (R0Buffer)                  DMAxDA  DMA0-Base + d!  
  #R0Buffer                   DMAxSZ  DMA0-Base + ! 
  0 key# !                    \ reset pointer to start of buffer    
  DMAEN                       DMAxCTL DMA0-Base + bis!        \ start dma
  UCRXIFG                     IFG     eUSCI-A0-Base + bic!    \ clear flag to confirm rx
  ['] term-key? hook-key? !
  ['] term-key  hook-key  !
 ;

: termrx ( -- ) \ reset terminal hooks as per old values
  DMAEN                       DMAxCTL DMA0-Base + bic! \ stop all dma 
  ['] serial-key? hook-key? !
  ['] serial-key  hook-key  !
;

\ Remember to chain init after reset

\ DMA terminal disabled
\ : init init
\   initrx
\ ;