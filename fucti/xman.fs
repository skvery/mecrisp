\ xman.fs  (work in progress )
\
\ Memory map
\
\   (high)
\ +---------+  HEAP.TOP
\ | Handle0 |  --+
\ | Handle1 |    |
\ | Handle2 |    |
\ | Handle3 |    |
\ |   ...   |    | <- Next.Handle
\ |    .    |    |
\ | dynamic |    |
\ |  free   |    |
\ |  space  |    |
\ |    .    |    |
\ |   ...   |    |
\ | +-----+ |    | <- Next.Piece
\ |         |    |
\ |  data1  |    |
\ |         |    |
\ | +-----+ |    |
\ |  size1  |    |
\ | +-----+ |    |
\ |         |    |
\ |  data0  |    |
\ |         |    |
\ | +-----+ |<---+
\ |  size0  |                       extended
\ +---------+ HEAP (redefined)      memory
\ | static  |     (for dynamic)     map
\ | xternal |
\ | memory  |
\ |         |
\ +---------+ HEAP (static)
\ +---------+
\ |         |
\ |  64K    |
\ |  low    |
\ | memory  |
\ |         |
\ +---------+  
\    (low)
\
\ External Dynamic Memory Manager
\ inspired by W B Dress (1985)
\ Next.Piece ! becomes NEXT.PIECE 2!
\ NEXT.PIECE becomes NEXT.PIECE 2@
\ TO NEXT.PIECE becomes NEXT.PIECE tox
: tox
  2@ xx!
;
\ AT NEXT.PIECE becomes NEXT.PIECE atx
: atx
  2@ xx@
;
\ Position of data in xmem can change dynamically and handle access is mandatory.
\ (Manual garbage collection is implemented using size=0 for returned handles and
\  handle=0 after garbage collection.)
include xmem.fs
\ (fist do all static allocations before calling xalloc)
\ Dynamic memory utilities
\ Initialize xman.fs by calling heap.init after all static allocations are completed
: xalloc ( -- )
  2 xalign,                            \ xsize requires alignment
  xhere 2constant HEAP                 \ HEAP redefined from xmem.fs
;
\ HEAP                                 \ see above - use heap.init
\ HEAP.TOP                             \ from xmem.fs
\ Next.Piece                           \ from xmem.fs
0. 2variable  Latest.Released          \ increased efficiency
\ Next.Handle                          \ from xmem.fs

\ Usage summary:
\ allocate a secion of memory
  \ : from.heap ( xsize --  Xhandle )
\ return memory using handle
  \ : to.heap ( Xhandle -- )
\ compact heap ( single thread or restart only )
  \ : compact.heap ( -- )
\ change size of allocated region
  \ : resize.handle ( Xhandle xsize -- )
\ todo: confirm fail-safe!
: handle.mem ( Xhandle -- xaddr )      \ return xaddr of memory
  2@ dx@                               
;
: handle.size ( Xhandle -- xsize )     \ return size of memory
  handle.mem xsz- dx@
;
: heap.init ( -- ) \ 0 fill ( do later with dma )
  xalloc
  HEAP.TOP swap drop
  HEAP swap drop
  do
    HEAP.TOP i =          \ if = (H) stop at HEAP.TOP(L)
    if else drop 0 then
    HEAP i =                 \ if = (H) start at HEAP(L)
    if else drop 0 then
    do
      0. i j dx!
      2
    +loop
  loop
;
\ #56 Low level management
                                        \ ?collision moved to xmem.fs
: scan.heap ( -- Xhandle )
  HEAP.TOP 4. d-                     \ start looking for empty handle
  begin 2dup dx@ d0=                                      \ is size 0
    if 4 0 d- else 2dup next.handle dx@ d= \ are we finished looking?
      if 4 0 d- next.handle dx@ dx@ dx+! then exit  \ with new handle
    then 2dup next.handle dx@ d=                       \ keep looking
  until 2dup off                            \ not found so create one
  2dup latest.released dx@ dx!          \ at next.handle while giving
    4 0 d-  2dup 4 0 d- next.handle dx@ dx!   \ a latest.released and
;                                  \ a new next.handle for efficiency
\ #57 Adjust handle contents after heap compaction
: bump.handles ( xsize xaddr -- )      \ Look for handles above point
;
: handle.size ( Xhandle -- xsize )
  2@ 2x@ xsz- 2x@
;
\ #58 Allocate a section of memory
: from.heap ( xsize -- Xhandle )
  2dup drop 1 and 0 d+                             \ ensure alignment
\  2dup ?collision if 2drop 0. exit then         \ no collision check
  latest.released d?dup             \ look here first - may save time
  if latest.released dx@ dx@ off                \ use latest.released
  else scan.heap then          \ Oops! Need to look for unused handle
;
\ #59 Return a handle and compact heap
: to.heap ( Xhandle -- )          \ compacting heap must block access
;
\ #60 Change size of allocated region
: resize.handle ( xhandle xsize -- flag ) \ 0 = ok
  2over 2@ xsz- 2over 2over       ( xhandle xsize psize xsize psize )
  dx@ d- xsz- ?collision                 ( xhandle xsize psize flag )
  if 2drop 2drop exit then                    ( xhandle xsize psize )
  2dup 2dup dx@ d+ xsz+ next.piece d=        \ avoid moving if on top
  if 2over 2over dx@ d- next.piece d+! dx! false exit then
  next.pice 2@ 3 2pick xsz+ 4. d/ 1. d+ 2dup 2>r
  from.heap 2over to.heap 2dup 2@ 2swap 2dup 0. latest.released 2!
  2r> 2over 2. d- dx! 2swap 2! 0
;
  
\ #62 Misc stuff useful for debugging
: ?handle ( Xhandle -- -1 if handle is current )
  next.handle drop    \ to get low part
  @ 4 + HEAP.TOP drop \ to get low part
  do


  4 +loop
;
\ heap.init