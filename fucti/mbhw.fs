\ mainMBusHW.fs
\
\ Forth UCT Project
\ 
\ main MBus hardware testing
\
\ Connect MBus and IEC1107 ports
\ (baud rate command on terminal)
\ (DMA Rx to Tx and Tx to Rx)
\
\ -------------------------------
\

eraseflash
compiletoflash
  
include setupdebug.fs
include teston.txt
\ include testoff.txt

: init ( -- )  \ init placeholder for cold startup configuration
   cr ." ...head of init"
;

\ constants
include setconstants.fs

\ io utilities
include setuputility.fs

\ extended memory utilities
include xmem.fs

\ see simple allocation for allocated extended memory

include setupX0000h.fs

include setuprtc.fs
include lpm5.fs

include setupepoch.fs
include setupmilli.fs
include wait.fs
include setupserial.fs

check
