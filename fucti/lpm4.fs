\ lpm4.fs
\
\ Low Power Mode 3.5 Demonstration Project
\ 
\ ( Wake up once per minute to sample Vbat and Temp )
\ ( To download power up and reset the device )
\
\ -------------------------------
eraseflash
compiletoflash
: lpm ( -- )              \ LPM
                          \ 32.768 watch calender
      0 4 $320 + c!       \ PxDIR PJ_Base - set port minimum consumption
  $FFFF 6 $320 + c!       \ PxREN PJ_Base - input with pull
    0 $0C $320 + c!       \ PJ_Base PxSEL1
  $30 $0A $320 + c!       \ PJ_Base PxSEL0
  $A5 1 $04A0 + c!        \ RTCKEY   RTCCTL0_H RTC_BASE - unlock
  $40 2 $04A0 + cbic!     \ RTCHOLD  RTCCTL1_L RTC_BASE - run
  $40 0 $04A0 + c!        \ RTCTEVIE RTCCTL0_L RTC_BASE
  $FF 1 $04A0 + c!        \ FF       RTCCTL0_H RTC_BASE - lock
  1 $10 $0120 + cbic!     \ LOCKLPM5 PMM5CTL0 PMM_BASE
  \ required before unlock
  \ rest of setup included here for conveniance
  $5A84 0 $015C + !       \ WDTPW-WDTHOLD WDTCTL WDT_BASE
  $A5 1 $0120 + c!        \ PMMPWD    PMMCTL0_H PMM_BASE - unlock
  $40 0 $0120 + cbic!     \ SVSHE     PMMCTL0_L PMM_BASE - clear
  $10 0 $0120 + cbis!     \ PMMREGOFF PMMCTL0_L PMM_BASE - set 
  $FF 1 $0120 + c!        \ FF        PMMCTL0_H PMM_BASE - lock
;
: on ( -- )
  1 2 $200 + cbis!        \ LED1 for diagnostic
  1 4 $200 + cbis!        \ LED1 for diagnostic
;
: usercode 
  ( -- )
  on
;
: golpm5 ( -- na )
  $0E $04A0 + @ drop   \ RTCIV RTC_BASE reset
  [ $D072 , $00F0 , ]  \ BIS.B #CPUOFF+OSCOFF+SCG0+SCG1, SR
; 
: preparelpm ( -- NA R: -- NA) \ prepare and enter lpm
  \ Reset all ports to minimise power consumption
  \ OUT, DIR, REN, SELx = 0 
    0 2 $200 + !             \ PxOUT  P1-2_Base
    0 2 $220 + !             \ PxOUT  P3-4_Base
    0 2 $240 + !             \ PxOUT  P5-6_Base
    0 2 $260 + !             \ PxOUT  P7-8_Base
    0 4 $200 + !             \ PxDIR  P1-2_Base
    0 4 $220 + !             \ PxDIR  P3-4_Base
    0 4 $240 + !             \ PxDIR  P5-6_Base
    0 4 $260 + !             \ PxDIR  P7-8_Base
$FFFC 6 $200 + !             \ PxREN  P1-2_Base ( P1.1 LED2 P1.0 LED1 )
$FFFF 6 $220 + !             \ PxREN  P3-4_Base
$FF9F 6 $240 + !             \ PxREN  P5-6_Base ( P5.6 BTN1 P5.5 BTN2 )
$FFFF 6 $260 + !             \ PxREN  P7-8_Base
  0 $1A $200 + !             \ PxIEN  P1-2_Base
  0 $1A $220 + !             \ PxIEN  P3-4_Base
  0 $1A $240 + !             \ PxIEN  P5-6_Base
  0 $1A $260 + !             \ PxIEN  P7-8_Base
  0 $0A $200 + !             \ PxSEL0 P1-2_base 
  0 $0C $200 + !             \ PxSEL1 P1-2_base
  0 $0A $220 + !             \ PxSEL0 P3-4_Base
  0 $0C $220 + !             \ PxSEL1 P3-4_Base
  0 $0A $240 + !             \ PxSEL0 P5-6_Base
  0 $0C $240 + !             \ PxSEL1 P5-6_Base
  0 $0A $260 + !             \ PxSEL0 P7-8_Base
  0 $0C $260 + !             \ PxSEL1 P7-8_Base
  \ Now do REN, for all unconnected
  \ PJ not changed - RTC uses LFXIN LFXOUT
  \ PJ is configured with RTC
  \ P1.0 --- led1
  \ P1.1 --- led2
  \ P1.2 ren j1.2
  \ P1.3 ren j1.6
  \ P1.4 ren j3.27
  \ P1.5 ren j3.28
  \ p1.6 ren sd_mosi na
  \ p1.7 ren sd_miso na
  \ p2.0 ren bcl_Tx jumper removed
  \ p2.1 ren bcl_Rx  jumper removed
  \ p2.2 ren sd_spiclk na
  \ p2.3 ren nc
  \ p2.4 ren nc
  \ p2.5 ren j4.34
  \ p2.6 ren j4.35
  \ p2.7 ren nc 
  \ p3.0 ren j3.23 
  \ p3.1 ren j3.24
  \ p3.2 ren j3.25
  \ p3.3 ren j3.26
  \ p3.4 ren j4.37
  \ p3.5 ren j4.38
  \ p3.6 ren j4.39
  \ p3.7 ren j4.40
  \ p4.0 ren sd_cs dnp
  \ p4.1 ren j4.31
  \ p4.2 ren j4.32
  \ p4.3 ren j4.33
  \ p4.4 ren j2.18
  \ p4.5 ren nc
  \ p4.6 ren nc
  \ p4.7 ren j3.29
  \ p5.0 ren j2.15
  \ p5.1 ren j2.14
  \ p5.2 ren j1.7
  \ p5.3 ren j2.17
  \ p5.4 ren nc
  \ p5.5 --- button2
  \ p5.6 --- button1
  \ p5.7 ren j2.19
  \ p6.0 ren j1.4 Tx
  \ p6.1 ren j1.3 Rx
  \ p6.2 ren j1.5 
  \ p6.3 ren j1.8
  \ p6.4 ren nc
  \ p6.5 ren nc
  \ p6.6 ren nc
  \ p6.7 ren nc
  \ p7.0 ren j1.10
  \ p7.1 ren j1.9
  \ p7.2 ren sd_detect dnp
  \ p7.3 ren j4.36
  \ p7.4 ren nc
  \ p7.5 ren nc
  \ p7.6 ren nc
  \ p7.7 ren nc
  \ p8.0 ren j3.30s
  \ p8.1 ren j2.11
  \ p8.2 ren j2.12
  \ p8.3 ren j2.13
  \ p8.4 ren nc
  \ p8.5 ren nc
  \ p8.6 ren nc
  \ p8.7 ren nc
  lpm
  golpm5
;
\ Mecrisp should branch here asap to conserve energy
\ minimum user setup code and back to sleep 
\ all peripheral initialisation to be handled by user
: lpminit ( -- ) \ called by Mecrisp if LPM5 start 
  \ retain 1 MHz MCLCK
  4 0 $04A0 + cbit@  \ RTCTEVIFG RTCCTL0_L RTC_BASE = RTCTEV 
  if
    begin on again
    lpm \ and clear LOCKLPM5
    usercode
    golpm5
  then
  reset
;
: init ;
\ Mecrisp should define init to either do lpminit or normal restart
\ ( code to be removed up after Mecrisp modification )
: init ( -- )  \ init placeholder for cold startup configuration
  \ if pmmlpm5ifg set
  $019E @ 8 =        \ SYSRSTIV = PMMLPM5 
  if
    lpminit
  then
  init  \ normal init here
;

\ preparelpm