\ lpm4.fs
\
\ Low Power Mode 3.5 Demonstration Project
\ 
\ ( Wake up once per minute to sample Vbat and Temp )
\ ( To download reset the device and use dump )
\ ( tlvdump can be used to get calibration values )
\
\ -------------------------------
  \ P1.0 --- led1
  \ P1.1 --- led2
  \ P1.2 ren j1.2
  \ P1.3 ren j1.6
  \ P1.4 ren j3.27
  \ P1.5 ren j3.28
  \ p1.6 ren sd_mosi na
  \ p1.7 ren sd_miso na
  \ p2.0 ren bcl_Tx jumper removed
  \ p2.1 ren bcl_Rx  jumper removed
  \ p2.2 ren sd_spiclk na
  \ p2.3 ren nc
  \ p2.4 ren nc
  \ p2.5 ren j4.34
  \ p2.6 ren j4.35
  \ p2.7 ren nc 
  \ p3.0 ren j3.23 
  \ p3.1 ren j3.24
  \ p3.2 ren j3.25
  \ p3.3 ren j3.26
  \ p3.4 ren j4.37
  \ p3.5 ren j4.38
  \ p3.6 ren j4.39
  \ p3.7 ren j4.40
  \ p4.0 ren sd_cs dnp
  \ p4.1 ren j4.31
  \ p4.2 ren j4.32
  \ p4.3 ren j4.33
  \ p4.4 ren j2.18
  \ p4.5 ren nc
  \ p4.6 ren nc
  \ p4.7 ren j3.29
  \ p5.0 ren j2.15
  \ p5.1 ren j2.14
  \ p5.2 ren j1.7
  \ p5.3 ren j2.17
  \ p5.4 ren nc
  \ p5.5 --- button2
  \ p5.6 --- button1
  \ p5.7 ren j2.19
  \ p6.0 ren j1.4 Tx
  \ p6.1 ren j1.3 Rx
  \ p6.2 ren j1.5 
  \ p6.3 ren j1.8
  \ p6.4 ren nc
  \ p6.5 ren nc
  \ p6.6 ren nc
  \ p6.7 ren nc
  \ p7.0 ren j1.10
  \ p7.1 ren j1.9
  \ p7.2 ren sd_detect dnp
  \ p7.3 ren j4.36
  \ p7.4 ren nc
  \ p7.5 ren nc
  \ p7.6 ren nc
  \ p7.7 ren nc
  \ p8.0 ren j3.30s
  \ p8.1 ren j2.11
  \ p8.2 ren j2.12
  \ p8.3 ren j2.13
  \ p8.4 ren nc
  \ p8.5 ren nc
  \ p8.6 ren nc
  \ p8.7 ren nc
  \ pJ.0 ren j3.30s
  \ pJ.1 ren j2.11
  \ pJ.2 ren nc
  \ pJ.3 ren nc
  \ pJ.4 --- lfxin
  \ pJ.5 --- lfxout
  \ pJ.6 ren nc
  \ pJ.7 ren nc
\ -------------------------------
eraseflash
compiletoflash
$D3FE constant BACKDOOR 
: reset5 ( -- )           \ LPM
  $FFFF 6 $200 + !        \ PxREN  P1-2_Base ( P1.1 LED2 P1.0 LED1 )
  $FFFF 6 $220 + !        \ PxREN  P3-4_Base
  $FFFF 6 $240 + !        \ PxREN  P5-6_Base ( P5.6 BTN1 P5.5 BTN2 )
  $FFFF 6 $260 + !        \ PxREN  P7-8_Base
  $FFFF 6 $320 + c!       \ PxREN PJ_Base - input with pull
    0 $0C $320 + c!       \ PJ_Base PxSEL1
  $30 $0A $320 + c!       \ PJ_Base PxSEL0
  $A5 1 $04A0 + c!        \ RTCKEY   RTCCTL0_H RTC_BASE - unlock
  $40 2 $04A0 + cbic!     \ RTCHOLD  RTCCTL1_L RTC_BASE - run
  $40 0 $04A0 + c!        \ RTCTEVIE RTCCTL0_L RTC_BASE
  $FF 1 $04A0 + c!        \ FF       RTCCTL0_H RTC_BASE - lock
  1 $10 $0120 + cbic!     \ LOCKLPM5 PMM5CTL0 PMM_BASE
  \ required before unlock
  \ rest of setup included here for conveniance
  \ $5A84 0 $015C + !     \ WDTPW-WDTHOLD WDTCTL WDT_BASE
  $A5 1 $0120 + c!        \ PMMPWD    PMMCTL0_H PMM_BASE - unlock
  $40 0 $0120 + cbic!     \ SVSHE     PMMCTL0_L PMM_BASE - clear
  $10 0 $0120 + cbis!     \ PMMREGOFF PMMCTL0_L PMM_BASE - set 
  $FF 1 $0120 + c!        \ FF        PMMCTL0_H PMM_BASE - lock
;
: on ( -- )
  1 2 $200 + cbis!        \ LED1 for diagnostic
  1 4 $200 + cbis!        \ LED1 for diagnostic
;
$0800 constant ADC12Base
  $00 constant ADC12CTL0
  $02 constant ADC12CTL1
  $04 constant ADC12CTL2
  $06 constant ADC12CTL3
  $0C constant ADC12IFGR0
  $0D constant ADC12IFGR0_H
  $12 constant ADC12IER0  
  $18 constant ADC12IV
  $2E constant ADC12MCTL7 
  $30 constant ADC12MCTL8
: adc12isr ( -- )
  0 ADC12IV ADC12Base + !            \ ADC12IV reset
  wakeup                             \ from lpm1 - lpm4 
;
\ ['] adc12isr irq-adc !
$10000. 2constant BUF                \ FRAM buffer 
$40000. 2constant PTR                \ pointer to tail of FRAM buffer
$40004. 2constant CNT                \ restart counter
$40008. 2constant TAG                \ crashfinder - use by tagging line numbers
$40010. 2constant PMM                \ FRAM buffer
$40020. 2constant RTC                \ FRAM buffer
: useradc ( -- )
  132 TAG x!
  $02A2 ADC12CTL1  ADC12Base + !     \ SHP ADC12DIV6 ADC12SSEL0 ADC12CONSEQ1 
  134 TAG x!
  $0001 ADC12CTL2  ADC12Base + bis!  \ ADC12RES2 ADC12PWRMD ADC12CTL2
  136 TAG x!
  $00C7 ADC12CTL3  ADC12Base + !     \ TCMAP BATMAP STARTADD07 < 1 MHz
  138 TAG x!
  $011E ADC12MCTL7 ADC12Base + !     \ ADC12VRSEL1          ADC12INCH1E ADC12MCTL7 
  140 TAG x!
  $019F ADC12MCTL8 ADC12Base + !     \ ADC12VRSEL1 ADC12EOS ADC12INCH1E ADC12MCTL8 
  142 TAG x!
  $0390 ADC12CTL0  ADC12Base + !     \ ADC12SHT03 ADC12MSC ADC12ON ADC12CTL0
  \ clear flags
  145 TAG x!
  0     ADC12IFGR0 ADC12Base + !     \ ADC12IFG0 clear 
  147 TAG x!
  0     ADC12IV    ADC12Base + !     \ ADC12IV reset
  149 TAG x!
  $0100 ADC12IER0  ADC12Base + !     \ ADC12IER0 enable
  151 TAG x!
  $0003 ADC12CTL0  ADC12Base + bis!  \ ADC12CTL0 start ADC
  154 TAG x!
  begin $01 ADC12IFGR0_H ADC12Base + cbit@ until     \ ADC12IFGR0_H done?
  \ ['] adc12isr irq-adc ! eint 155 TAG x! lpm2 dint ['] nop irq-adc !
  \ 155 TAG x! lpm1
  156 TAG x!
  $0006 ADC12CTL1  ADC12Base + bic!  \ ADC12CTL1 single channel
  158 TAG x!
  $0002 ADC12CTL0  ADC12Base + bic!  \ ADC12CTL0 stop ADC
  160 TAG x!
;
: usersave ( -- )
  \ save $10 bytes to FRAM
  PTR x@ 0 BUF d+ 2>r
  PTR x@ $10 + PTR x!     \ ptr inc to next sample saved
  $10                     \ length 
  2r@ x!                  \        saved
  $10 $04A0 + @           \ minute second RTCTIM0
  2r@ 4. d+ x!            \               saved
  $12 $04A0 + @           \ dow hour RTCTIM1
  2r@ 2. d+ x!            \          saved
  $6E $0800 + @           \ ADC12MEM7 
  2r@ 6. d+ x!            \           saved
  $70 $0800 + @           \ ADC12MEM8
  2r> 8. d+ x!            \           saved
;
: usercode ( -- )
  useradc
  usersave
;
: lpm5 ( -- na )
  \ 0 $1E $0180 + !
  $0E $04A0 + @ drop      \ RTCIV RTC_BASE reset
  [ $D072 , $00F0 , ]     \ BIS.B #CPUOFF+OSCOFF+SCG0+SCG1, SR
;
: portsoff ( -- )
  \ Reset all ports to minimise power consumption
  \ OUT, DIR, REN, SELx = 0 
    0 4 $200 + !             \ PxDIR  P1-2_Base
    0 4 $220 + !             \ PxDIR  P3-4_Base
    0 4 $240 + !             \ PxDIR  P5-6_Base
    0 4 $260 + !             \ PxDIR  P7-8_Base
    0 4 $320 + c!            \ PxDIR  PJ_Base
    0 2 $200 + !             \ PxOUT  P1-2_Base
    0 2 $220 + !             \ PxOUT  P3-4_Base
    0 2 $240 + !             \ PxOUT  P5-6_Base
    0 2 $260 + !             \ PxOUT  P7-8_Base
    0 2 $320 + c!            \ PxOUT  PJ_Base
  0 $1A $200 + !             \ PxIEN  P1-2_Base
  0 $1A $220 + !             \ PxIEN  P3-4_Base
  0 $1A $240 + !             \ PxIEN  P5-6_Base
  0 $1A $260 + !             \ PxIEN  P7-8_Base
  0 $0A $200 + !             \ PxSEL0 P1-2_base 
  0 $0C $200 + !             \ PxSEL1 P1-2_base
  0 $0A $220 + !             \ PxSEL0 P3-4_Base
  0 $0C $220 + !             \ PxSEL1 P3-4_Base
  0 $0A $240 + !             \ PxSEL0 P5-6_Base
  0 $0C $240 + !             \ PxSEL1 P5-6_Base
  0 $0A $260 + !             \ PxSEL0 P7-8_Base
  0 $0C $260 + !             \ PxSEL1 P7-8_Base
;
: initPTR ( -- )             \ to reset pointer
  0 PTR x! 
;
: golpm5 ( -- NA R: -- NA)   \ prepare and enter lpm
  0. CNT xx!
  initptr
  portsoff 
  reset5
  lpm5
;
$0160 constant CSBase
  $01 constant CSLOCK 
  $04 constant CSCTL2
  $A5 constant CSKEY
: modclk ( -- )
  CSKEY CSLOCK CSBase + c!   \ KEY CSCTL0_H \ unlock CS
  $24   CSCTL2 CSBase + c!   \ SELS2 SELM4 CSCTL2_L \ SMCLK-LFMODCLK MCLK-MODCLK
  $FF   CSLOCK CSBase + c!   \ LOCK CSCTL0_H
;
: wakeup5 ( -- )             \ backdoor for cold startup configuration
  CNT xx@ drop 2 + dup $E >
  if drop 0 then 0 CNT xx!   \ inc CNT
  $1E $0180 + @              \ SYSRSTIV = PMMLPM5
  dup 
  0 $04A0 + @                \ RTCTEVIFG RTCCTL0_L RTC_BASE = RTCTEV
  RTC CNT xx@ d+ x!
  PMM CNT xx@ d+ x!
  0 $1E $0180 + !            \ SYSRSTIV clear all
  0 $0E $04A0 + !            \ RTCIV clear all
  \ and 
  8 =
  if
    modclk                
    portsoff
    reset5                   \ and clear LOCKLPM5 to reenable ports
    useradc
    usersave
    lpm5
  then
;
\ lpm5 reset and wakeup!
['] wakeup5 BACKDOOR flash!    
: dump ( -- )
  cr
  PTR x@ 16 / 0 ?do 
    i 16 * hex. 
    8 0 ?do
      j 16 * i 2 * + 0 BUF d+ x@ hex. 
    loop
    cr
  loop
;
: tlvdump ( -- )             \ to read cal values into spreadsheet
  cr
  $1A00 dup 70 + swap ?do 
    i dup hex. c@ hex.  
    cr
  loop
;
: gg ( -- )                  \ debug ADC
  initptr
  modclk
  portsoff                   
  begin 
    reset5 
    usercode 
    0 0 do 10 0 do nop loop loop 
  again 
;
: init ( -- )
  cr ." CNT = " CNT xx@ drop hex. cr
  eint
  $10 0 do i hex. 
    ."  PMM = " PMM i 0 d+ x@ hex.
    ."  RTC = " RTC i 0 d+ x@ hex.
  cr
  2 +loop
;