\ lpm35demo.fs
\
\ Low Power Mode 3.5 Backdoor Demo with RTC
\ -----------------------------------------
eraseflash
compiletoflash
$D3FE constant BACKDOOR
: rtc ( -- )           \ 32.768 kHz with calender mode
      \ all ports need setup here for minimum consumption
      0   4 $320 + c!     \ PxDIR PJ_Base - set port minimum consumption
  $FFFF   6 $320 + c!     \ PxREN PJ_Base - input with pull
      0 $0C $320 + c!     \ PJ_Base PxSEL1 - lfxt
    $30 $0A $320 + c!     \ PJ_Base PxSEL0 - lfxt
    $A5   1 $04A0 + c!    \ RTCKEY   RTCCTL0_H RTC_BASE - unlock
    $00   0 $04A0 + c!    \ RTCTEVIE RTCCTL0_L RTC_BASE - enable
    $0020 2 $04A0 + !     \ RTCMODE  RTCCTL13 RTC_BASE - calender
    $FF   1 $04A0 + c!    \ FF       RTCCTL0_H RTC_BASE - lock
    $1A $0A $04A0 + c!    \ RTC/128 RTCPS1CTL_L RTC_BASE - enable
      1 $10 $0120 + cbic! \ LOCKLPM5 PMM5CTL0 PMM_BASE - unlock ports
    $5A84 0 $015C + !     \ WDTPW-WDTHOLD WDTCTL WDT_BASE - watchdog off
    $A510 0 $0120 + !     \ PMMPWD PMMREGOFF PMMCTL0      - sel lpm5
      $FF 1 $0120 + c!    \ FF               PMMCTL0_H - lock
      0 $0E $04A0 + !     \ RTCIV - clear
;
: lpm5 ( -- na )          \ lpm5 power off
  [ $D072 , $00F0 , ]     \ BIS.B #CPUOFF+OSCOFF+SCG0+SCG1, SR
; 
: wakeup5 ( -- infty )  \ init placeholder for cold startup configuration
  begin again
;
: go5
  rtc
  ['] wakeup5 BACKDOOR flash!    \ lpm5 reset and wakeup!
  lpm5                    \ CPU core power down
;