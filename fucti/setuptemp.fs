\ setuptemp.fs

eUSCI-A3-Base constant A3

: h ( -- )
  .0 PxDIR P6_BASE + cbis!      \ Set P6.0 to output direction
  .1 PxDIR P6_BASE + cbic!      \ Set P6.1 to input direction
  .1 PxREN P6_BASE + cbic!      \ Set P6.1 to hi-z
  .1 PxIN P6_BASE + cbit@       \ Read pin 
  .0 PxOUT P6_BASE + cbis!      \ Write pin
  .1 PxIN P6_BASE + cbit@       \ Read pin
  . .
;

: l ( -- )
  .0 PxDIR P6_BASE + cbis!      \ Set P6.0 to output direction
  .1 PxDIR P6_BASE + cbic!      \ Set P6.1 to input direction
  .1 PxREN P6_BASE + cbic!      \ Set P6.1 to hi-z
  .1 PxIN P6_BASE + cbit@
  .0 PxOUT P6_BASE + cbic! 
  .1 PxIN P6_BASE + cbit@
  . .
;

: showtasks ( -- )
  cr cr
  boot-task  hex.  ." Task0 - boot-task" cr
  task1 hex.  ." Task1 - debug-task" cr
  \ task2     @ hex.  ." Task2 - remote-task" cr
\  task3  hex.  ." Task3 - local-task" cr
  tasks
  cr
;

: s ( -- )
  .0 PxDIR P6_BASE + cbic!
  .1 PxREN P6_BASE + cbic!      \ Set P6.1 to hi-z
  $A5 TXBUF eUSCI-A3-Base + !
;

: ss ( -- ) s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s s ;