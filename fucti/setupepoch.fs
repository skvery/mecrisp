\ setupepoch.fs

\ We will need a way to ensure that writing epoch values are not corrupted during power down.
\ This is done by writing a second copy and tagging it with a check of the low word.

: copyEpoch0  ( -- )         \ copy Epoch0 to Epoch1
  Epoch0-HH dx@ Epoch1-HH dx! 
;

: copyEpoch1  ( -- )         \ copy Epoch1 to Epoch0
  Epoch1-HH dx@ Epoch0-HH  dx! 
  Epoch1-LL x@ Epoch-CC    x!
;

: sec? ( -- u)
  Epoch0-LL x@
;

: 2sec? ( -- 2ud)
  Epoch0-HH dx@
;

: epoch-isr ( -- )           \ serves epoch counter in fail-safe way - low power mode?                           
  1. Epoch0-HH dx+!          \ increment double Epoch0-HH:Epoch0-LL
  Epoch0-LL x@ Epoch-CC x!    \ copy Epoch0-LL to EpochCC
  0 RTC_BASE RTCIV + !       \ write interrupt vector to reset 
  \ eint                     \ it is now safe to use Epoch0
  copyEpoch0                 \ save a copy to detect power-down failure 
  toggle2                    \ from utility toggle green LED 
;
\ At power-up detect and overwrite incorrect value
: initepoch
  Epoch0-LL x@ Epoch-CC x@ = if copyEpoch0 else copyEpoch1 then
   verbose @ if cr ." ...setupeopch init" then
   ['] epoch-isr irq-rtc !
   eint
   verbose @ if cr ." eint..." then
;
: init init
  initepoch
;
