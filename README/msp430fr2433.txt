;------------------------------------------------------------------------------
; Specials for MSP430FR2433:
;------------------------------------------------------------------------------

        irq-port1       ( -- a-addr ) Memory locations for IRQ-Hooks
        irq-port2
        irq-adc
        irq-uscia0
        irq-uscia1
        irq-uscib0
        irq-watchdog
        irq-rtc
        irq-timera0                   Timer 0 CCR0
        irq-timera1                   Timer 0 CCR1, CCR2, TAIV
        irq-timerb0                   Timer 1 CCR0
        irq-timerb1                   Timer 1 CCR1, CCR2, TAIV
        irq-timerc0                   Timer 2 CCR0
        irq-timerc1                   Timer 2 CCR1, TAIV
        irq-timerd0                   Timer 3 CCR0
        irq-timerd1                   Timer 3 CCR1, TAIV

