;------------------------------------------------------------------------------
; Specials for MSP430G2955, MSP430G2855, MSP430G2755:
;------------------------------------------------------------------------------

                                                 Reads analog-digital-converter
        analog          ( u-channel -- u-value )   with 2.5 V reference voltage
        adc-1.5         ( u-channel -- u-value )   with 1.5 V reference voltage
        adc-vcc         ( u-channel -- u-value )   with Vcc as reference

        irq-port1       ( -- a-addr ) Memory locations for IRQ-Hooks
        irq-port2
        irq-adc
        irq-timera1
        irq-timera0
        irq-watchdog
        irq-timerb1
        irq-timerb0
        irq-rx
        irq-tx
        irq-comp
        irq-timerc1
        irq-timerc0

