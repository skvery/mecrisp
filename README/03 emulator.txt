;------------------------------------------------------------------------------
; Emulator
;------------------------------------------------------------------------------

The assembler source can be assembled with
Alfred Arnolds *great* macro assembler AS,
which source is included here to have everything in one place.
Install it, if you like to dive into Mecrisp.

Run the assemble bash skript or use this command for assembling:
asl forth-mecrisp.asm && p2hex forth-mecrisp.p -r 0x0000-0xffff

;------------------------------------------------------------------------------

Included you will find my MSP430 CPU emulator Mecrimemu,
if you want to try this out on your x86 linux box.

Its Freepascal source is included under GPL too,
but for now, it is a native german speaking tool.

There are some bash scripts to easily invoke it:

mecrisp                    Simply runs Forth
mecrisp-definitions        Runs Forth and feeds in your definitions

mecrisp-image-definitions  Runs Forth, makes a blank flash dictionary image
                           and feeds definitions in
mecrisp-image              Runs Forth with flash dictionary image in place

mecrisp-memmap             Runs Forth, feeds in ramdefinitions.txt and
                           shows disassembler listing for the
                           executed parts in ram dictionary
                           and a ram memory map.

