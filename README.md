# Welcome to Fraf, #

the Forth readme and files!

This is a documented experience of getting started with Mecrisp Forth and other things sometimes related to Forth.

### How does it work? ###

* The files are stored here in the Fraf
* The experience is documented in [twoF](../../wiki/Home), the wiki of Forth

Please note that this is a work in progress starting with the G2553 launchpad and working later with the FR5994 launchpad.  (Most of the code will also be applicable to the FR6998 launchpad.)

### Documentation links ###

* [MSP430FR5994 Data Sheet](http://www.ti.com/lit/ds/symlink/msp430fr5994.pdf)
* [MSP430FR68xx, and MSP430FR69xx Family User's Guide](http://www.ti.com/lit/ug/slau367m/slau367m.pdf)


### How do I get involved? ###

* I am not sure at this stage... Log an issue please.

### Who do I talk to? ###

* Log issues and I will try to fix or update.
* First try with SourceTree (SourceTree is working)