\ setupdma.fs

\ UCA0 - P2.0 and P2.1 Launchpad diagnostic port ( 115200 bd )  - DMA0rx
\ UCA1 - P2.5 and P2.6 Boosterpack pins ( 9600 bd or 19200 bd ) - DMA1rx and DMA2tx
\ UCA2 - P5.4 and P5.5 (P5.4 nc on Launchpad)
\ UCA3 - P6.0 and P6.1 Boosterpack UART ( 300 bd and 2400 bd )  - DMA3rx and DMA4tx
\ DMA5 used for checksum calculations and ( block transfers? )

\ TRIGGER  CHANNEL 0  CHANNEL 1  CHANNEL 2  CHANNEL 3  CHANNEL 4  CHANNEL 5  
\                                                                  ______
\       0   DMAREQ     DMAREQ     DMAREQ     DMAREQ     DMAREQ    |DMAREQ|
\          _________
\      14 |UCA0RXIFG| UCA0RXIFG  UCA0RXIFG  UCA2RXIFG  UCA2RXIFG  UCA2RXIFG
\          
\      15  UCA0TXIFG  UCA0TXIFG  UCA0TXIFG  UCA2TXIFG  UCA2TXIFG  UCA2TXIFG
\                     _________             _________
\      16  UCA1RXIFG |UCA1RXIFG| UCA1RXIFG |UCA3RXIFG| UCA3RXIFG  UCA3RXIFG
\                                _________             _________ 
\      17  UCA1TXIFG  UCA1TXIFG |UCA1TXIFG| UCA3TXIFG |UCA3TXIFG| UCA3TXIFG


\ DMACTL0 - DMA1TSEL DMA0TSEL
\ DMACTL1 - DMA3TSEL DMA2TSEL
\ DMACTL2 - DMA5TSEL DMA4TSEL


\ Configure DMA

$0990 constant CRC16DIW0
$0998 constant CRC16INIRESW0
1 variable dma5free       \ semaphore

: dmacrc16 ( dsize xaddr -- )   \ setup dma5 for crc16 - burst-block to allow CPU
  DMAEN                        DMAxCTL DMA5-Base + bic! \ stop dma 
  $1F 8 lshift                 DMACTL2 bic!
  \ 0  8 lshift                DMACTL2 bis!    
  $2300                       
  \ DMADT = 010b (Burst-block) DMASRCINCR = 11b  7 DMADSTBYTE = 0  6 DMASRCBYTE = 0                          
                              DMAxCTL DMA5-Base + !
  CRC16DIW0                   DMAxDA  DMA5-Base + !
                              DMAxSA  DMA5-Base + d! \ from stack
  d2/ drop                    DMAxSZ  DMA5-Base + !  \ no of bytes on stack
  $FFFF                       CRC16INIRESW0 !
  DMAEN                       DMAxCTL DMA5-Base + bis!  
  DMAREQ                      DMAxCTL DMA5-Base + bis!  \ trigger
  ;


: crc16 ( dsize xaddr -- crc16 )
  dma5free wait                  \ wait for semaphore
  dmacrc16           
  DMAEN DMAxCTL DMA5-Base +  waitflagreset
  CRC16INIRESW0 @
  dma5free signal                \ clear semaphore
;

: dma5copy ( dsize sourceaddr destinationaddr -- )   \ setup dma5 for copy - burst-block to allow CPU
  DMAEN                        DMAxCTL DMA5-Base + bic! \ stop dma 
  $1F 8 lshift                 DMACTL2 bic!
  \ 0  8 lshift                DMACTL2 bis!    
  $2F00                       
  \ DMADT = 010b (Burst-block) DSTINCR|SRCINCR = 1111b  7 DMADSTBYTE = 0  6 DMASRCBYTE = 0                          
                              DMAxCTL DMA5-Base + !
                              DMAxDA  DMA5-Base + d! \ from stack
                              DMAxSA  DMA5-Base + d! \ from stack
  d2/ drop                    DMAxSZ  DMA5-Base + !  \ no of bytes on stack (word count)
  DMAEN                       DMAxCTL DMA5-Base + bis!  
  DMAREQ                      DMAxCTL DMA5-Base + bis!  \ trigger
  ;
: copy16 ( dsize sourceaddr destinationaddr -- )
  dma5free wait                  \ wait for semaphore
  dma5copy          
  DMAEN DMAxCTL DMA5-Base +  waitflagreset
  dma5free signal                \ clear semaphore
;


: cdmauca0rx ( -- ) \ setup dma0 for rx on uca0 with repeated single transfer
  DMAEN                       DMAxCTL DMA0-Base + bic! \ stop dma 
  $1F                         DMACTL0 bic!
  14                          DMACTL0 bis!    
  $4C80                       \ DMADT = 100b DMADSTINCR = 11b 7 DMADSTBYTE = 1  6 DMASRCBYTE = 0                          
                              DMAxCTL DMA0-Base + !
  eUSCI-A0-Base RXBUF +       DMAxSA  DMA0-Base + !
;

: cdmauca1rx ( -- ) \ setup dma1 for rx on uca1
  DMAEN                       DMAxCTL DMA1-Base + bic! \ stop dma 
  $1F 8 lshift                DMACTL0 bic!
  16  8 lshift                DMACTL0 bis!    
  $0C80                       \ DMADSTINCR = 11b 7 DMADSTBYTE = 1  6 DMASRCBYTE = 0                          
                              DMAxCTL DMA1-Base + !
  eUSCI-A1-Base RXBUF +       DMAxSA  DMA1-Base + !
;

: cdmauca1tx ( -- ) \ setup dma2 for tx on uca1
  DMAEN                       DMAxCTL DMA2-Base + bic! \ stop dma 
  $1F                         DMACTL1 bic!
  17                          DMACTL1 bis!    
  $0340                       \ DMASRCINCR = 11b  7 DMADSTBYTE = 0  6 DMASRCBYTE = 1                          
                             DMAxCTL DMA2-Base + !
  eUSCI-A1-Base TXBUF +      DMAxDA  DMA2-Base + !
  ;

: cdmauca3rx ( -- ) \ setup dma3 for rx on uca3
  DMAEN                       DMAxCTL DMA3-Base + bic! \ stop dma 
  $1F 8 lshift                DMACTL1 bic!
  16  8 lshift                DMACTL1 bis!    
  $0C80                       \ DMADSTINCR = 11b 7 DMADSTBYTE = 1  6 DMASRCBYTE = 0                          
                              DMAxCTL DMA3-Base + !
  eUSCI-A3-Base RXBUF +       DMAxSA  DMA3-Base + !
;

: cdmauca3tx ( -- ) \ setup dma4 for tx on uca3
  DMAEN                       DMAxCTL DMA4-Base + bic! \ stop dma 
  $1F                         DMACTL2 bic!
  17                          DMACTL2 bis!
  $0340                       \ DMASRCINCR = 11b  7 DMADSTBYTE = 0  6 DMASRCBYTE = 1                          
                              DMAxCTL DMA4-Base + !
  eUSCI-A3-Base TXBUF +       DMAxDA  DMA4-Base + !
;

\ DMARMWDIS required for multitasking
: cdmactl4 ( -- ) \ setup dmactl4 to set DMARMWDIS
  .2                          DMACTL4 bis!
;
\ DMA enable and start

: dmauca0rx ( daddr -- )      \ address of buffer - including size
  DMAEN                       DMAxCTL DMA0-Base + bic!        \ stop dma 
  2dup 2. d+                  DMAxDA  DMA0-Base + d!  
  x@                          DMAxSZ  DMA0-Base + ! 
                                                             
  DMAIFG                      DMAxCTL DMA0-Base + bic!        \ DMA complete
  DMAEN                       DMAxCTL DMA0-Base + bis!        \ start dma
  UCRXIFG                     IFG     eUSCI-A0-Base + bic!
;
                            
: dmauca1rx ( daddr -- )      \ address of buffer - including size
  DMAEN                       DMAxCTL DMA1-Base + bic!        \ stop dma 
  2dup 2. d+                  DMAxDA  DMA1-Base + d!  
  x@                          DMAxSZ  DMA1-Base + ! 
                                                              \ enable interrupt?
  DMAIFG                      DMAxCTL DMA1-Base + bic!        \ DMA complete
  DMAEN                       DMAxCTL DMA1-Base + bis!        \ start dma
  UCRXIFG                     IFG     eUSCI-A0-Base + bic!
;
                            
: dmauca1tx ( daddr -- )      \ address of buffer - including size
  DMAEN                       DMAxCTL DMA2-Base + bic!        \ stop dma 
  2dup 2. d+                  DMAxSA  DMA2-Base + d!  
  x@                          DMAxSZ  DMA2-Base + ! 
                                                              \ enable interrupt?
  DMAIFG                      DMAxCTL DMA2-Base + bic!        \ DMA complete
  DMAEN                       DMAxCTL DMA2-Base + bis!        \ start dma
  UCTXIFG                     IFG     eUSCI-A1-Base + bic!    \ trigger first Tx
  UCTXIFG                     IFG     eUSCI-A1-Base + bis!    \ trigger first Tx
;
                            
: dmauca3rx ( daddr -- )      \ address of buffer - including size
  2dup 2. d+                  DMAxDA  DMA3-Base + d!  
  x@                          DMAxSZ  DMA3-Base + ! 
                                                              \ enable interrupt?
  DMAIFG                      DMAxCTL DMA3-Base + bic!        \ DMA complete
  DMAEN                       DMAxCTL DMA3-Base + bis!        \ start dma
  UCRXIFG                     IFG     eUSCI-A3-Base + bic!    \ clear flag
;

: dmauca3tx ( daddr -- )      \ address of buffer - including size - usage (p3array) dmauca3tx
  DMAEN                       DMAxCTL DMA4-Base + bic!        \ stop dma 
  2dup 2. d+                  DMAxSA  DMA4-Base + d!  
  x@                          DMAxSZ  DMA4-Base + ! 
                                                              \ enable interrupt?
  DMAIFG                      DMAxCTL DMA4-Base + bic!        \ DMA complete
  DMAEN                       DMAxCTL DMA4-Base + bis!        \ start dma
  UCTXIFG                     IFG     eUSCI-A3-Base + bic!    \ trigger first Tx 
  UCTXIFG                     IFG     eUSCI-A3-Base + bis!    \ trigger first Tx
  DMAIFG     DMAxCTL DMA4-Base + waitflagset                  \ wait for DMA to complete
;

: uca3tx ( Array -- )   \ array with length as first entry
  dmauca3tx
;
: crc16s          ( dn xaddr -- ) \ crc16 to be saved at xaddr + n  
  2>r 2dup        ( dn dn r: xaddr ) 
  2r@ crc16       ( dn crc r: xaddr ) 
  rot rot         ( crc d r: xaddr ) 
  2r> d+          ( crc sxaddr ) 
  x!              ( -- ) 
;                          
: crc16f ( dn xaddr -- flag )  
  2>r 2dup        ( dn dn r: xaddr ) 
  2r@ crc16       ( dn crc r: xaddr ) 
  rot rot         ( crc d r: xaddr ) 
  2r> d+          ( crc sxaddr ) 
  x@ =
;

\ (S0) x@ (S0) crc16s \ calculate and save S0 checksum
\ (S0) x@ (S0) crc16f \ calculate S0 checksum flag

\ (S2) x@ (S2) crc16s \ calculate and save S2 checksum
\ (S2) x@ (S2) crc16f \ calculate S2 checksum flag
\ For Query line n

: qcrc16s ( i -- )
  q#length 2 - 0 rot q#length um* (Q) d+ crc16s
;

: qcrc16f ( i -- flag )
  q#length 2 - 0 rot q#length um* (Q) d+ crc16f
;


: init init
  cdmactl4
  0 qcrc16s
  1 qcrc16s
  2 qcrc16s
;