\ xmem.fs
\ Extended memory utilities
\ ( for memory map refer to xman.fs )
\ include teston.txt or testoff.txt
t{ 1 0 -> 1 0 }t
t{ compiletoram align 1 2 here 2! here 2@ compiletoflash -> 1 2 }t
\ Write address register using extended instruction
\ : 22! ( d addr -- ) 
\  [ 
\  $4437 ,  
\  $1800 , 
\  $44F7 , 
\  $0000 ,
\  ]
\ ;
t{ compiletoram align 7 1 here 0 xx! here 0 xx@ compiletoflash -> 65543. }t
: 2x! ( d/ud xaddr -- )
  2dup 2>r x! 2r> 2. d+ x!
;
: 2x@ ( xaddr -- d/ud )
  2dup 2>r  2. d+ x@ 2r> x@
;
t{ compiletoram align 1 2 here 2! here 0 2x@ compiletoflash -> 1 2 }t
t{ compiletoram align 1 2 here 0 2x! here 2@ compiletoflash -> 1 2 }t
t{ compiletoram align 7 $11 here 0 2x! here 0 2x@ compiletoflash -> 7 $11 }t
\ Write long address register using extended instruction
\ use this instruction to write xaddr to low memory as required by TI
\ word sequence in MSP430 double register assignment is not the same as Mecrisp
: d! ( xadr addr -- )
  0 xx!
; t{ 8 1 DMAxDA DMA0BASE + d! DMAxDA DMA0BASE + @ 0 DMAxDA DMA0BASE + ! -> 8 }t
\ write to low memory
: 22! ( d/ud adr -- )
  0 2x!
; t{ compiletoram align 9 8 here 22! here 2@ compiletoflash -> 9 8 }t 
: 2x+! ( ud|d xaddr -- )  \ increment extended double memory 
  2dup 2>r 2x@ d+ 2r> 2x!
; t{ compiletoram align 6 5 here 0 2x! 2 1 here 0 2x+! here 0 2x@ compiletoflash -> 
: 2+! ( ud|d addr -- )  \ increment double memory 
  dup >r 2@ d+ r> 2!
; t{ compiletoram align 6 5 here 2! 2 1 here 2+! here 2@ compiletoflash -> 8 6 }t
\ Extend normal utility functions
: x+! ( u xaddr -- )
  2dup 2>r x@ + 2r> x!
; t{ compiletoram 7 here 0 x! 2 here 0 x+! here 0 x@ compiletoflash -> 9 }t
: 2and ( d d -- d )
  rot and -rot and swap
; t{ 5 3 5 3 2and -> 5 3 }t t{ 5 1 3 3 2and -> 1 1 }t
: d?dup ( d -- 0. | d d )
  2dup d0= not if 2dup then
; t{ 0. d?dup ->  0 0 }t  t{ 1 0 d?dup -> 1 0 1 0 }t t{ 0 1 d?dup -> 0 1 0 1 }t
: sz- ( -- ) 2 0 d-                  \ length of size word also used in xman.fs
; t{ 5 5 sz- -> 3 5 }t
: sz+ ( -- ) 2 0 d+                  \ length of size word also used in xman.fs
; t{ 5 5 sz+ -> 7 5 }t
: xsz- ( -- ) 4 0 d-                  \ length of xsize word also used in xman.fs
; t{ 5 5 xsz- -> 1 5 }t
: xsz+ ( -- ) 4 0 d+                  \ length of xsize word also used in xman.fs
; t{ 5 5 xsz+ -> 9 5 }t
\ Simple static xmemory allocation define start of xmem
$10000. 2constant HEAP                \ redefined by xman.fs
$44000. 2constant HEAP.TOP            \ 256K MSP430FRxxx4
\ $24000. 2constant HEAP.TOP            \ 128K MSP430FRxxx2
\ $1C000. 2constant HEAP.TOP            \ 96K MSP430FRxxx2
\ $14000. 2constant HEAP.TOP            \ 64K MSP430FRxxx2
HEAP.TOP xsz- 2variable Next.Handle   \ refer to xman.fs
HEAP          2variable Next.Piece    \ refer to xman.fs
\ xalloc defined in xman.fs ends all static allocations
: xhere  ( -- xaddr ) 
  Next.Piece 2@
;
: xallot ( size -- ) 
  0  \ double 
  Next.Piece 2+!
;
\ to initialize xmem use as follows:
\   2 xalign
\   xhere 2constant (name)
\   1 1 2x,  1 x, 1 x, [char] % cx, 
\   2 xalign, 
\   xhere 2constant (name)
\   2 xbuffer,
\ ?collision is true if no room is available - no automatic checks are performed!
: ?collision ( size -- flag )              \ will allotting size cause failure?
  0  \ double
  Next.Piece 2@ d+ xsz+ Next.Handle 2@ d>
; 
t{ Next.Handle 2@ 8 0 d- Next.Piece 2dup 2>r 2! 5 ?collision 2r> Next.Piece 2! -> true }t
t{ Next.Handle 2@ 8 0 d- Next.Piece 2dup 2>r 2! 4 ?collision 2r> Next.Piece 2! -> false }t
: xbuffer, ( size -- )
  xallot                                          
; t{ Next.Piece 2@ 2dup 2>r $F1F1 0 d+ $F1F1 xbuffer, Next.Piece 2@ d= 2r> Next.Piece 2! -> true }t
: xalign, ( size -- )                            \ align the next allot to the n-boundary 
  0  \ double 
  2dup 1. d- Next.Piece 2@ 2and ( xsize xoffset ) \ ( no check for 2^ )
  2dup d0=
  if                                              \ on n-boundary        
    2drop 2drop
  else
    d- drop xallot                                \ add xsize - xoffset
  then
; t{ Next.Piece 2@ 2>r $F1F1 1 Next.Piece 2! $100 xalign, Next.Piece 2@ $F200 1 d= 2r> Next.Piece 2! -> true }t 
: 64Kalign, ( -- )                            \ align the next allot to 64K 
  64K  \ double 
  2dup 1. d- Next.Piece 2@ 2and ( xsize xoffset ) \ ( no check for 2^ )
  2dup d0=
  if                                              \ on n-boundary        
    2drop 2drop
  else
    d- drop xallot                                \ add xsize - xoffset
  then
;
: x, ( u -- )
  2 xalign,
  Next.Piece 2@ x!
  2 xallot
; t{ 1234 x, -2. Next.Piece 2+! Next.Piece 2@  x@ -> 1234 }t
: 2x, ( d -- )
  2 xalign,
  next.piece 2@ 2x!
  4 xallot
; t{ 1234 1234 2x, -4. Next.Piece 2+! Next.Piece 2@  2x@ -> 1234 1234 }t
: cx, ( c -- )
  next.piece 2@ cx!
  1 xallot
; t{ 12 cx, -1. Next.Piece 2+! Next.Piece 2@  cx@ -> 12 }t
t{ HEAP Next.Piece 2! Next.Piece 2@ HEAP d= -> true }t      \ reset Handle
: resetbuffer ( u xaddr -- )   \ usage #name (name) reset counted buffer 
  x!
;
\ # Optional assembler definition of 22!
\  Wortbirne Flag_visible, "22!" ; Store ( d addr -- ) using mov.a
\    popda r7 ; Fetch address
\    word 1800h,44F7h,0000h
\  ret
\ see 22!
\ FD26: 4437  mov.w @r4+, r7
\ FD28: 1800  Unknown Opcode .w r0
\ FD2A: 44F7  mov.b @r4+, 0h(r7)
\ FD2C: 0000
\ FD2E: 4130  mov.w @r1+, r0
