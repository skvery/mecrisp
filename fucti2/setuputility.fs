\ setuputility.fs
\ Set up LED and Switches for diagnostic purposes.

: toggle1 ( -- )             \ Toggles P1.0
  .0 PxOUT P1Base + cxor!
;

: toggle2 ( -- )             \ Toggles P1.1
  .1 PxOUT P1Base + cxor!
;

: s1? ( -- flag )            \ S1 active?
  .6 PxIN P5Base + cbit@ not
;

: s2? ( -- flag )            \ S2 active?
  .5 PxIN P5Base + cbit@ not
;
: initutility
\ Set P1.0 to LED output direction
    .0 PxDIR  P1Base + cbis!
    .0 PxSEL0 P1Base + cbic!
    .0 PxSEL1 P1Base + cbic!
  
\ Set P1.1 to LED output direction
    .1 PxDIR  P1Base + cbis! 
    .1 PxSEL0 P1Base + cbic!
    .1 PxSEL1 P1Base + cbic!
  
\ Set P5.5 to input direction
  .5 PxDIR P5Base + cbic!

\ Set P5.6 to input direction
  .6 PxDIR P5Base + cbic!

\ Set P5.5 to high
  .5 PxOUT P5Base + cbis!

\ Set P5.6 to high
  .6 PxOUT P5Base + cbis!

\ Set P5.5 to resitor pull
  .5 PxREN P5Base + cbis!

\ Set P5.6 to resitor pull
  .6 PxREN P5Base + cbis!
;
: init init
  initutility
;
