\ setupserial.fs

\ Serial ports

\ UCA0 - P2.0 and P2.1 Launchpad diagnostic port ( 115200 bd )
\ UCA1 - P2.5 and P2.6 MBus ( 9600 bd or 19200 bd )
\ UCA2 - P5.4 and P5.5 (P5.4 nc on Launchpad)
\ UCA3 - P6.0 and P6.1 IEC1107 UART ( 300 bd and 2400 bd )
\ UCA3 - RxEn needs P6.2 high


\ BRCLK   Baud Rate UCOS16 UCBRx UCBRFx UCBRSx
\ 
\ 8000000    300         1  1666     10   0xD6
\ 8000000    600         1   833      5   0x49
\ 0000000   1200         1   416     10   0xD6
\ 8000000   2400         1   208      5   0x49
\ 8000000   4800         1   104      2   0xD6
\ 8000000   9600         1    52      1   0x49
\ 8000000  19200         1    26      0   0xD6
\ 8000000 115200         1     4      5   0x55

$01 constant UCSWRST       \ byte software reset
$80 constant SMCLK         \ byte SMCLK select

1666 constant BRW300
 833 constant BRW600
 416 constant BRW1200
 208 constant BRW2400
 104 constant BRW4800
  52 constant BRW9600
  26 constant BRW19200
   4 constant BRW115200

$D6A1 constant BRSF300
$4951 constant BRSF600
$D6A1 constant BRSF1200
$4951 constant BRSF2400
$D621 constant BRSF4800
$4911 constant BRSF9600
$D601 constant BRSF19200
$5551 constant BRSF115200

: uca3config300 ( -- )  \ 300 bd 7e1
  UCSWRST               eUSCIA3Base CTLW0 + !    \ stop UCA
  .15 .14 or .12 or     \ parity enable even parity seven bit data
  SMCLK or              eUSCIA3Base CTLW0 + bis!
  BRW300                eUSCIA3Base BRW   + !
  BRSF300               eUSCIA3Base MCTLW + !
  
  .0 .1 or              P6Base PxSEL0 + cbis!
  .0 .1 or              P6Base PxSEL1 + cbic!
  .2                    P6Base PxOUT  + cbis!     \ RxEn
  .2                    P6Base PxDIR  + cbis!
 
  UCSWRST               eUSCIA3Base CTLW0 + bic!  \ start UCA
;
: uca3config2400 ( -- ) \ 2400 bd 7e1 2stop
  UCSWRST               eUSCIA3Base CTLW0 + !    \ stop UCA
  .15                   \ parity enable
  .14 or                \ even parity
  .12 or                \ seven bit data
  SMCLK or              eUSCIA3Base CTLW0 + bis!
  BRW2400               eUSCIA3Base BRW   + !
  BRSF2400              eUSCIA3Base MCTLW + !
  
  .0 .1 or              P6Base PxSEL0 + cbis!
  .0 .1 or              P6Base PxSEL1 + cbic!
  .2                    P6Base PxOUT  + cbis!     \ RxEn
  .2                    P6Base PxDIR  + cbis!

  UCSWRST               eUSCIA3Base CTLW0 + bic!  \ start UCA
;
: uca3config9600 ( -- ) \ 9600 bd 8n1
  UCSWRST               eUSCIA3Base CTLW0 + !    \ stop UCA
  .15                   \ parity enable
  .14 or                \ even parity
  .12 or                \ seven bit data
  SMCLK or              eUSCIA3Base CTLW0 + bis!
  BRW9600               eUSCIA3Base BRW   + !
  BRSF9600              eUSCIA3Base MCTLW + !
  
  .0 .1 or              P6Base PxSEL0 + cbis!
  .0 .1 or              P6Base PxSEL1 + cbic!
  .2                    P6Base PxOUT  + cbis!     \ RxEn
  .2                    P6Base PxDIR  + cbis!

  UCSWRST               eUSCIA3Base CTLW0 + bic!  \ start UCA
;

: uca1config300 ( -- )  \ 300 bd 8n1
  UCSWRST               eUSCIA1Base CTLW0 + !    \ stop UCA
  SMCLK                 eUSCIA1Base CTLW0 + bis!
  BRW300                eUSCIA1Base BRW   + !
  BRSF300               eUSCIA1Base MCTLW + !
  
  .5 .6 or              P2Base PxSEL0 + cbic!
  .5 .6 or              P2Base PxSEL1 + cbis!
  
  UCSWRST               eUSCIA1Base CTLW0 + bic!  \ start UCA
;
: uca1config2400 ( -- ) \ 2400 bd 8n1
  UCSWRST               eUSCIA1Base CTLW0 + !    \ stop UCA
  SMCLK                 eUSCIA1Base CTLW0 + bis!
  BRW2400               eUSCIA1Base BRW   + !
  BRSF2400              eUSCIA1Base MCTLW + !
  
  .5 .6 or              P2Base PxSEL0 + cbic!
  .5 .6 or              P2Base PxSEL1 + cbis!

  UCSWRST               eUSCIA1Base CTLW0 + bic!  \ start UCA
;
: uca1config9600 ( -- ) \ 19200 bd 8n1
  UCSWRST               eUSCIA1Base CTLW0 + !    \ stop UCA
  SMCLK                 eUSCIA1Base CTLW0 + bis!
  BRW9600               eUSCIA1Base BRW   + !
  BRSF9600              eUSCIA1Base MCTLW + !
  
  .5 .6 or              P2Base PxSEL0 + cbic!
  .5 .6 or              P2Base PxSEL1 + cbis!

  UCSWRST               eUSCIA1Base CTLW0 + bic!  \ start UCA
;
: uca1config19200 ( -- ) \ 19200 bd 8n1
  UCSWRST               eUSCIA1Base CTLW0 + !    \ stop UCA
  SMCLK                 eUSCIA1Base CTLW0 + bis!
  BRW19200              eUSCIA1Base BRW   + !
  BRSF19200             eUSCIA1Base MCTLW + !
  .5 .6 or              P2Base PxSEL0 + cbic!
  .5 .6 or              P2Base PxSEL1 + cbis!

  UCSWRST               eUSCIA1Base CTLW0 + bic!  \ start UCA
;

: uca0config115200 ( -- ) \ 115200 bd 8n1
  UCSWRST               eUSCIA0Base CTLW0 + !    \ stop UCA
  SMCLK                 eUSCIA0Base CTLW0 + bis!
  BRW115200             eUSCIA0Base BRW   + !
  BRSF115200            eUSCIA0Base MCTLW + !
  
  .0 .1 or              P2Base PxSEL0 + cbic!
  .0 .1 or              P2Base PxSEL1 + cbis!

  UCSWRST               eUSCIA0Base CTLW0 + bic!  \ start UCA
;

