\ setuprtc.fs

\ PJSEL0.4 and PJSEL0.5 = 1
\ PJSEL1.4 and PJSEL1.5 = 0
\ LFXT BYPASS = 0

\ RTCMODE = 1 calender mode but only using 1 s ticks
\ RTCTEV = $00 event on minute changed

\ The RTCPWD register implements key protection and controls the lock or unlock state of the module.
\ When this register is written with correct key, 0A5h, the module is unlocked and unlimited write access
\ possible to RTC_C registers. After the module is unlocked, it remains unlocked until the user writes any
\ incorrect key or until the module is reset.

\ RTCCTL1
%01000000 constant RTCHOLD           \ 6 RTCHOLD RW 1h 

\ RTCPS1CTL
\ %0000000000000000 constant RT1IP     \ 4-2 RT1IPx RW 0h 110b = Divide by 2 for debugging...
%0000000000011000 constant RT1IP     \ 4-2 RT1IPx RW 0h 110b = Divide by 128 for 1 Hz
%0000000000000010 constant RT1PSIE   \ 1 RT1PSIE RW 0h 

\ Real-Time Clock Interrupt Vector Register
\ 0Eh = Interrupt Source: RTC prescaler 1; Interrupt Flag: RT1PSIFG
\ For interrupt handler refer to 23.2.6.1 RTCIV Software Example

\ Enter LPM3.5 as follows:
\
\ bic #RTCHOLD, &RTCCTL13
\ bis #PMMKEY + REGOFF, &PMMCTL0
\ bis #LPM4, SR

\ 4. An LPM3.5 wake-up event like an edge on a wake-up input pin or an RTC_C interrupt event starts the
\ BOR entry sequence and the core voltage regulator. All peripheral registers are set to their default
\ conditions. The I/O pin state and the interrupt configuration for the RTC_C remain locked.
\ 5. The device can be configured. The I/O configuration and the RTC_C interrupt configuration that was
\ not retained during LPM3.5 should be restored to the values that they had before entering LPM3.5.
\ Then the LOCKLPM5 bit can be cleared, which releases the I/O pin conditions and the RTC_C
\ interrupt configuration. Registers that are retained during LPM3.5 should not be altered before
\ LOCKLPM5 is cleared.
\ 6. After enabling I/O and RTC_C interrupts, the interrupt that caused the wake-up can be serviced.

: initrtc ( -- )                       \ init from LPM
                                       \ For 32.768 watch crystal
      0 PxDIR PJBase + c!             \ set port for minimum consumption
  $FFFF PxREN PJBase + c!             \ input with high pullup
  $FFFF PxOUT PJBase + c!
        0 PJBase PxSEL1 + c!
  .4 .5 + PJBase PxSEL0 + c!

  PWD_RTC RTCBASE RTCPWD + c!         \ unlock
  RT1IP RT1PSIE or RTCBASE RTCPS1CTL + !

  RTCHOLD RTCBASE RTCCTL1 + cbic!     \ 4. Clear the RTCHOLD bit to start the counters.
  0 RTCBASE RTCPWD + c!               \ lock
;


: init init
  initrtc
;


  