\ mainMBusIEC.fs
\
\ Forth UCT Project
\ 
\ main MBus hardware testing
\
\ Connect MBus and IEC1107 ports
\ (baud rate command on terminal)
\ (DMA Rx to Tx and Tx to Rx)
\
\ -------------------------------
\

eraseflash
compiletoflash
  
include setupdebug.fs
include teston.txt
\ include testoff.txt

: init ( -- )  \ init placeholder for cold startup configuration
   cr ." ...head of init"
;

\ constants
include setconstants.fs

\ io utilities
include setuputility.fs

\ extended memory utilities
include xmem.fs

\ see simple allocation for allocated extended memory

include setupX0000h.fs

include setuprtc.fs
\ include lpm5.fs
\ reset

include setupepoch.fs
include setupmilli.fs
include wait.fs
include setupserial.fs

: b3 ( -- )  \ change throughput to 300 bd
  uca1config300
  uca3config300
;
: b24 ( -- ) \ change throughput to 2400 bd
  uca1config2400
  uca3config2400
;
: b96 ( -- ) \ \ change throughput to 9600 bd
  uca1config9600
  uca3config9600
;

: b96-3 ( -- ) \ \ change throughput 9600 bd - 300 bd
  uca1config9600
  uca3config300
;

\ include multitask.txt

\ ======================================
\
\ UCA1 - P2.5 (Tx) and P2.6 (Rx) Boosterpack pins ( 9600 bd ) - DMA1rx and DMA2tx
\
\ UCA3 - P6.0 (Tx) and P6.1 (Rx) Boosterpack UART ( 300 bd )  - DMA3rx and DMA4tx
\ (see http://www.ti.com/lit/ml/slau677/slau677.pdf and https://www.adafruit.com/product/954)
\ The red lead should NOT be connected, it will blow the CPU!  (The TI board 3.3V PSU to be used from the debug USB.)
\ The black lead to any GND
\ The white lead to TXD on P2.5
\ The green lead to RXD on P2.6
\ P6.0 is connected to the red LED (J7 bottom pin)

\ Use any utility to see the LED flicker.  Command snoop follows the DMA buffer.

\ include setupdma.fs DMA code goes here
\ TRIGGER  CHANNEL 0  CHANNEL 1  CHANNEL 2  CHANNEL 3  CHANNEL 4  CHANNEL 5  
\                                                                  
\                     _________             _________
\      16  UCA1RXIFG |UCA1RXIFG| UCA1RXIFG |UCA3RXIFG| UCA3RXIFG  UCA3RXIFG

\ DMACTL0 - DMA1TSEL DMA0TSEL
\ DMACTL1 - DMA3TSEL DMA2TSEL

\ Configure DMA

$0990 constant CRC16DIW0
$0998 constant CRC16INIRESW0

: cdmauca1rx ( -- ) \ setup dma1 for rx1 single tranfer to tx3
  DMAEN                       DMAxCTL DMA1Base + bic! \ stop dma 
  $1F 8 lshift                DMACTL0 bic!
  16  8 lshift                DMACTL0 bis!    
  $0000                       \ DMADSTINCR = 00b 7 DMADSTBYTE = 0  6 DMASRCBYTE = 0                          
                              DMAxCTL DMA1Base + !
  eUSCIA1Base RXBUF +         DMAxSA  DMA1Base + !
  eUSCIA3Base TXBUF +         DMAxDA  DMA1Base + !
  65535                       DMAxSZ  DMA1Base + !
  DMAIFG                      DMAxCTL DMA1Base + bic!        \ DMA complete
  DMAEN                       DMAxCTL DMA1Base + bis!        \ start dma
  UCRXIFG                     IFG     eUSCIA0Base + bic!    \ clear rx flag
;

: cdmauca3rx ( -- ) \ setup dma3 for rx3 single tranfer to tx1
  DMAEN                       DMAxCTL DMA3Base + bic! \ stop dma 
  $1F 8 lshift                DMACTL1 bic!
  16  8 lshift                DMACTL1 bis!    
  $0000                       \ DMADSTINCR = 00b 7 DMADSTBYTE = 0  6 DMASRCBYTE = 0                          
                              DMAxCTL DMA3Base + !
  eUSCIA3Base RXBUF +         DMAxSA  DMA3Base + !
  eUSCIA1Base TXBUF +         DMAxDA  DMA3Base + !
  65535                       DMAxSZ  DMA1Base + !
  DMAIFG                      DMAxCTL DMA3Base + bic!        \ DMA complete
  DMAEN                       DMAxCTL DMA3Base + bis!        \ start dma
  UCRXIFG                     IFG     eUSCIA3Base + bic!    \ clear rx flag
;


\ DMARMWDIS required for multitasking
: cdmactl4 ( -- ) \ setup dmactl4 to set DMARMWDIS
  .2                          DMACTL4 bis!
;
\ DMA enable and start
: init  
  init cr ." ...tail of init"
  cdmactl4
  b96-3
  cdmauca1rx
  cdmauca3rx
;

: snoop
  0 0
  begin
    2>r                         ( x1 x3 --             R:       -- x1 x3 )
    DMAxSZ DMA1Base +           (       -- a1          R: x1 x3 -- x1 x3 )
    DMAxSZ DMA3Base +           ( a1    -- a1 a3       R: x1 x3 -- x1 x3 )
    swap @                      ( a1 a3 -- a3 y1       R: x1 x3 -- x1 x3 )  
    swap @                      ( a3 y1 -- y1 y3       R: x1 x3 -- x1 x3 )
    2r>                         ( y1 y3 -- y1 y3 x1 x3 R: x1 x3 -- )
    2 pick <> if over hex. then ( y1 y3 x1 x3 -- y1 y3 x1 ) 
    2 pick <> if over hex. then ( y1 y3 x1 -- y1 y3 ) 
    key?
  until
  2drop                         ( y1 y3 -- )
;                            
\ run
check
init

