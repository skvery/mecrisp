\ setupmilli.fs

\ this is an approximate milli second clock 2.4 % fast 32768/32 Hz)

\ PJ setup for crystal

\ setup milli                 \ sequence:  clear, set dividers, set TBCLR, set direction
\
: initmilli
  MC_RESET                      TB0BASE T_CTL + !                     \ stop the timer
  TBIDEX8                       TB0BASE T_EX0 + bis!
  ID4                           TB0BASE T_CTL + bis!                  \ divider done
  TBCLR                         TB0BASE T_CTL + bis!                  \ reset divider
  TBSSEL MC_CM or               TB0BASE T_CTL + bis!                  \ start the timer
;
: init init
  initmilli
;
\ add getmilli
: milli? ( -- u )
  TB0BASE T_R + @
  begin
    TB0BASE T_R + @    \ read again to ensure reliable async read
    tuck
    <>
  until                \ end when =
;



