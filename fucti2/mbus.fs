\ mainMBusHW.fs
\
\ Forth UCT Project
\ 
\ main MBus hardware
\
\ 9600 baud, even parity, one stop bit (9600e1)
\ DMA Rx and Tx
\ Master <--->Interface<--->Slave
\             unit 0        unit 1  
\ -------------------------------
\ MBus data blocks supported as follows:
\
\ DIF       | DIFE      | VIF |VIFE | ... description
\ ESFF DDDD | ...       |             Extension,Storage,Function,Data ( storage = 0 for live data )
\ 1XXX XXXX | EUTT SSSS |             Extension,Unit,Tariff,Storage ( unit = 0 is for Fucti - not extended ) ( unit = 1 for IEC1107 port )
\ 0000 0100 |           | 0010 0100 | Operating time-seconds
\ 0000 1101 |           | 0111 1001 | 0000 1000 8 x byte secondary address and ID field ( read only )
\ 0000 0001 |           | 0111 1010 | primary address 
\ 0000 0010 |           | 1111 1101 | 0001 1100 16 bit baud rate for unit 0 (can only be written if unit 1 baud rate is $ffff
\ 0000 1101 |           | 1111 1101 | 0110 0111 |  n x byte special information (Forth string read and write)
\ 
\ planned for slave
\ 1000 1110 | 0100 0000 | 0000 0100 | energy 0,01 kWh as 12 digit BCD for slave device
\ 1000 0100 | 0100 0000 | 0010 0000 | On-time for slave device on IEC1107 port
\ 1000 1101 | 0100 0000 | 0111 1000 | 1100 0111 14-digit BCD serial number of device on slave port
\ 1000 1100 | 0100 0000 | 1111 1101 | 0000 0001 0,01 credit units (kWh)
\ 1000 0001 | 0100 0000 | 1111 1101 | 0001 1011 8 x binary status includes load switch
\ 1000 0010 | 0100 0000 | 1111 1101 | 0001 1100 16 bit baud rate for unit 1 (see also unit 0)
\ 1000 1101 | 0100 0000 | 1111 1101 | 0110 0111 |  n x byte special information (slave write and read via IEC 1107)
\
\ length(1) serial(7) time(4) kWh(6) credit(4) status(1) checksum(2) = 26 bytes rolling buffer in 64 K gives almost 31 days 15 min stored as user data
\ the following will inform the cyclic buffer: First storage (time) Last storage (time) Size of storage block (number of records)
\ 0000 0100 |           | 1111 1101 | 0010 0000 | 32 bit oldest storage time in seconds
\ 0000 0100 |           | 1111 1101 | 0010 0001 | 32 bit last storage time in seconds
\ 0000 0010 |           | 1111 1101 | 0010 0010 | 16 bit number of samples in storage
\ 0000 0010 |           | 1111 1101 | 0010 0100 | 16 bit storage interval in seconds
\ 0000 0100 |           | 1111 1101 | 0010 1100 | 32 bit last storage time read in seconds - use this to change start time for readout
\ 0000 0100 |           | 0111 0100 | sync-offset time in seconds for capturing of profile data 
\ 0000 1111 |           | 0111 1111 | read manufacturer specific information - rolling buffer in 64 K giving almost 30 days of 15 min stored as user data

\ First conceptual draft of MBUS address decoding.  Next step to debug.

\ Decode MBus Data-link Layer

2constant MBRXBUF      \ pointer MBus rx buffer in xmem
2constant MBSADDR      \ pointer to MBus secondary address in xmem 
var mbpaddr            \ MBus primary address
var mbrxdp             \ data pointer to xmem 
var mbrxhp             \ head pointer to xmem
var mbrxlen            \ L = L byte
var MBTXBUF            \ pointer in xmem
var mbrxsem            \ semaphore to hand values to application layer
var mbreply            \ respond flag
var mbstatus           \ status to remember secondary addressing mode


: mbchecksum ( cs xx len - flag )      \ verifies mbus summated checksum
  0 do 2dup 2>r cx@ + 2r> 1 0 d+ loop  \ summate
  cx@ =
;


: hnibble? ( h1 h2 -- flag )                   \ returns true if the high nibble address match
  over                                         ( h1 h2 h1 ) 
  $f0 = 
  if 
    2drop 1                                    ( flag )
  else
    xor not                                    ( flag )
  then
;


: mbsaddr ( -- flag )                  \ mbus secondary address wildcard matching                                   
  1                                            ( flag )
  mbrxlen @ 3
  do
    MBRXBUF i 0 d+ cx@                         ( flag char1 )
    dup $ff = 
    if
      2drop 1                                  ( flag )
    else
      MBSADDR i 3 - 0 d+ cx@                   ( flag char1 char2 )
      over $f0 and over $f0 and                ( flag char1 char2 h1 h2 )
      hnibble? not if 2drop drop 0 leave then  ( flag )
      4 shl swap 4 shl                         ( flag h1 h2 )
      hnibble? not if drop 0 leave then        ( flag )
    then 
  loop
;                                              ( flag )    
   

: mbpaddr?  ( -- flag )                \ mbus primary address matching
  1 mbreply !
  MBRXBUF mbrxhp @ 1+ 0 d+ cx@
  case ( n -- n )
    253 of mbsaddr? swap endof
    254 1 swap endof
    255 0 mbreply ! 1 swap endof
    dup mbpaddr @ = ?of 1 swap endof
  endcase ( flag n -- flag )
; 

0        \ state n 
begin    \ state machine
  mbrx? pause else 1 mbrxdp +!  \ inc mbrxdp here and case must be called after each rx
   
  case ( n -- n )
     0  of MBRXBUF mbrxdp @ 0 d+ cx@ 10 = if mbrxdp @ 1+ mbrxhp ! drop 10 endof                                               \ after 10 start
     10 of mbrxdp @ mbrxhp @ - 2 = if drop 11 endof                                                                           \ wait for two more
     11 of 0 MBRXBUF mbrxhp @ 0 d+ 2 mbchecksum if drop 12 else drop 0 then endof                                             \ checksum ok
     12 of mbrxdp @ mbrxhp @ - 3 = if drop 13 endof                                                                           \ wait for one more
     13 of MBRXBUF mbrxdp @ 0 d+ cx@ 16 = if drop 100 else drop 0 then endof                                                  \ 16 end? to addr
     
     0  of MBRXBUF mbrxdp @ 0 d+ cx@ 68 = if mbrxdp @ 1+ mbrxhp ! drop 68 endof                                               \ after 68 start
     68 of mbrxdp @ mbrxhp @ - 1 = if drop 69 endof                                                                           \ wait for one more         
     69 of MBRXBUF mbrxhp @ 0 d+ 2dup 2>r cx@ dup mbrxlen ! r> r> 1+ swap cx@ = if mbrxdp @ 1+ mbrxhp ! drop 70 else drop 0 then endof   \ L=L
     70 of mbrxdp @ mbrxhp @ = if drop 71 then endof                                                                          \ wait for one more
     71 of MBRXBUF mbrxdp @ 0 d+ cx@ 68 = if mbrxdp @ 1+ mbrxhp ! drop 72 else  drop 0 then endof                             \ 68?  
     72 of mbrxdp @ mbrxhp @ - mbrxlen @ = if drop 73 then endof                                                              \ wait for L more
     73 of 0 MBRXBUF mbrxhp @ 0 d+ mbrxlen @ mbchecksum if drop 74 else drop 0 then endof                                     \ checksum ok
     74 of mbrxdp @ mbrxhp @ - 1- mbrxlen @ = if drop 75 then endof                                                           \ wait for one more
     75 of MBRXBUF mbrxdp @ 0 d+ cx@ 16 = if drop 680 else drop 0 then endof                                                  \ 16 end? addr

     100 of paddr? if 101 else drop 0 then endof
     680 of paddr? if 681 else drop 0 then endof
     

     true ?of dup endof                                                                                                         \ prepare for endcase (leave case on stack)
   endcase ( n n -- n )

again

