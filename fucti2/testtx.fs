\ testtx.fs

: testtx ( -- )
	milli?
	dup milli? swap -
	0 eUSCIA3Base IFG + !			\ clear ifg
	eUSCIA3Base IFG + @
	swap cr u. hex.
	cr
  0 eUSCIA3Base TXBUF + !   \ write txbuf
  begin .1 eUSCIA3Base IFG + bit@ until
  0 eUSCIA3Base TXBUF + !   \ write txbuf
  begin
	  dup milli? swap - dup
		eUSCIA3Base IFG + @
		dup if 0 eUSCIA3Base IFG + !			\ clear ifg
		then
  	swap cr u. hex.
		100 > until
;