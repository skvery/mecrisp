\ lpm5.fs
\
\ Low Power Mode 3.5
\ 
\ Fucti to go to lpm3.5 on power fail
\ and update epoch until power restore
\ normal operation in PFTIME seconds
\ -------------------------------
3 constant PFCNT
0 1 2constant CNT
  \ P1.0 ren nc
  \ P1.1 ren nc
  \ P1.2 ren nc
  \ P1.3 ren nc
  \ P1.4 ren nc
  \ P1.5 ren nc
  \ p1.6 ren nc
  \ p1.7 ren nc
  \ p2.0 --- TxMecrisp
  \ p2.1 --- RxMecrisp
  \ p2.2 ren nc
  \ p2.3 ren nc
  \ p2.4 ren nc
  \ p2.5 --- TxMBus
  \ p2.6 --- RxMBus
  \ p2.7 ren nc 
  \ p3.0 ren nc 
  \ p3.1 ren nc
  \ p3.2 ren nc
  \ p3.3 ren nc
  \ p3.4 ren nc
  \ p3.5 ren nc
  \ p3.6 ren nc
  \ p3.7 ren nc
  \ p4.0 ren nc
  \ p4.1 --- PFb (j4.31)
  \ p4.2 --- PFb (j4.32)
  \ p4.3 ren nc
  \ p4.4 ren nc
  \ p4.5 ren nc
  \ p4.6 ren nc
  \ p4.7 ren nc
  \ p5.0 ren nc
  \ p5.1 ren nc
  \ p5.2 ren nc
  \ p5.3 ren nc
  \ p5.4 ren nc
  \ p5.5 ren nc 
  \ p5.6 ren nc
  \ p5.7 ren nc
  \ p6.0 --- TxIR
  \ p6.1 --- RxIR
  \ p6.2 --- RxEnIR 
  \ p6.3 ren nc
  \ p6.4 ren nc
  \ p6.5 ren nc
  \ p6.6 ren nc
  \ p6.7 ren nc
  \ p7.0 ren nc
  \ p7.1 ren nc
  \ p7.2 ren nc
  \ p7.3 ren nc
  \ p7.4 ren nc
  \ p7.5 ren nc
  \ p7.6 ren nc
  \ p7.7 ren nc
  \ p8.0 ren nc
  \ p8.1 ren nc
  \ p8.2 ren nc
  \ p8.3 ren nc
  \ p8.4 ren nc
  \ p8.5 ren nc
  \ p8.6 ren nc
  \ p8.7 ren nc
  \ pJ.0 ren nc
  \ pJ.1 ren nc
  \ pJ.2 ren nc
  \ pJ.3 ren nc
  \ pJ.4 --- lfxin
  \ pJ.5 --- lfxout
  \ pJ.6 ren nc
  \ pJ.7 ren nc
\ -------------------------------
$D3FE constant BACKDOOR 
: reset5 ( -- )                 \ LPM
  $FFFF PxREN P1Base + !        \ PxREN  P1-2_Base
  $FFFF PxREN P3Base + !        \ PxREN  P3-4_Base
  $FFFF PxREN P5Base + !        \ PxREN  P5-6_Base
  $FFFF PxREN P7Base + !        \ PxREN  P7-8_Base
  $FFFF PxREN PJBase + c!       \ PxREN  PJ_Base
     0 PxSEL1 PJBase + c!      \ PJ_Base PxSEL1
   $30 PxSEL0 PJBase + c!      \ PJ_Base PxSEL0
  $A5 RTCPWD   RTCBase + c!        \ RTCKEY   RTCCTL0_H RTC_BASE - unlock
  $40 RTCCTL1  RTCBase + cbic!     \ RTCHOLD  RTCCTL1_L RTC_BASE - run
  $40 RTCCTL0  RTCBase + c!        \ RTCTEVIE RTCCTL0_L RTC_BASE
  $FF RTCPWD   RTCBase + c!        \ FF       RTCCTL0_H RTC_BASE - lock
    1 PMM5CTL0 PMMBase + cbic!     \ LOCKLPM5 PMM5CTL0 PMM_BASE
  \ required before unlock
  \ rest of setup included here for conveniance
  \ $5A84 0 $015C + !     \ WDTPW-WDTHOLD WDTCTL WDT_BASE
  $A5 PMMLOCK  PMMBase + c!        \ PMMPWD    PMMCTL0_H PMM_BASE - unlock
  $40 PMMCTL0  PMMBase + cbic!     \ SVSHE     PMMCTL0_L PMM_BASE - clear
  $10 PMMCTL0  PMMBase + cbis!     \ PMMREGOFF PMMCTL0_L PMM_BASE - set 
  $FF PMMLOCK  PMMBase + c!        \ FF        PMMCTL0_H PMM_BASE - lock
;
: usercode ( -- )
\  useradc
\  usersave
;
: lpm5 ( -- na )
  \ 0 $1E $0180 + !
  $0E RTCBase + @ drop      \ RTCIV RTC_BASE reset
  [ $D072 , $00F0 , ]     \ BIS.B #CPUOFF+OSCOFF+SCG0+SCG1, SR
;
: portsoff ( -- )
  \ Reset all ports to minimise power consumption
  \ OUT, DIR, REN, SELx = 0 
    0 4 $200 + !             \ PxDIR  P1-2_Base
    0 4 $220 + !             \ PxDIR  P3-4_Base
    0 4 $240 + !             \ PxDIR  P5-6_Base
    0 4 $260 + !             \ PxDIR  P7-8_Base
    0 4 $320 + c!            \ PxDIR  PJ_Base
    0 2 $200 + !             \ PxOUT  P1-2_Base
    0 2 $220 + !             \ PxOUT  P3-4_Base
    0 2 $240 + !             \ PxOUT  P5-6_Base
    0 2 $260 + !             \ PxOUT  P7-8_Base
    0 2 $320 + c!            \ PxOUT  PJ_Base
  0 $1A $200 + !             \ PxIEN  P1-2_Base
  0 $1A $220 + !             \ PxIEN  P3-4_Base
  0 $1A $240 + !             \ PxIEN  P5-6_Base
  0 $1A $260 + !             \ PxIEN  P7-8_Base
  0 $0A $200 + !             \ PxSEL0 P1-2_base 
  0 $0C $200 + !             \ PxSEL1 P1-2_base
  0 $0A $220 + !             \ PxSEL0 P3-4_Base
  0 $0C $220 + !             \ PxSEL1 P3-4_Base
  0 $0A $240 + !             \ PxSEL0 P5-6_Base
  0 $0C $240 + !             \ PxSEL1 P5-6_Base
  0 $0A $260 + !             \ PxSEL0 P7-8_Base
  0 $0C $260 + !             \ PxSEL1 P7-8_Base
;
: golpm5 ( -- NA R: -- NA)   \ prepare and enter lpm
  PFCNT CNT x!
  portsoff 
  reset5
  lpm5
;
: modclk ( -- )
  CSKEY CSLOCK CSBase + c!   \ KEY CSCTL0_H \ unlock CS
  $24   CSCTL2 CSBase + c!   \ SELS2 SELM4 CSCTL2_L \ SMCLK-LFMODCLK MCLK-MODCLK
  $FF   CSLOCK CSBase + c!   \ LOCK CSCTL0_H
;
: wakeup5 ( -- )             \ backdoor for cold startup configuration
  CNT x@ 1 + dup $E >
  if drop 0 then 0 PFCNT 2x!   \ inc CNT
  $1E $0180 + @              \ SYSRSTIV = PMMLPM5
  dup 
  0 $04A0 + @                \ RTCTEVIFG RTCCTL0_L RTC_BASE = RTCTEV
  ( interim RTC PFCNT xx@ d+ x! )
  ( interim PMM PFCNT xx@ d+ x! )
  0 $1E $0180 + !            \ SYSRSTIV clear all
  0 $0E $04A0 + !            \ RTCIV clear all
  \ and 
  8 =
  if
    modclk                
    portsoff
    reset5                   \ and clear LOCKLPM5 to reenable ports
( interim    useradc )
( interim    usersave )
    lpm5
  then
;
\ lpm5 reset and wakeup!
['] wakeup5 BACKDOOR flash!    
( interim : dump ( -- )
\  cr
\  PTR x@ 16 / 0 ?do 
\    i 16 * hex. 
\    8 0 ?do
\      j 16 * i 2 * + 0 BUF d+ x@ hex. 
\    loop
\    cr
\  loop
\ ;
: tlvdump ( -- )             \ to read cal values into spreadsheet
  cr
  $1A00 dup 70 + swap ?do 
    i dup hex. c@ hex.  
    cr
  loop
;
: gg ( -- )                  \ debug ADC
  initptr
  modclk
  portsoff                   
  begin 
    reset5 
  Ä  usercode 
    0 0 do 10 0 do nop loop loop 
  again 
;
: init ( -- )
  \ cr ." Power fail count = " PFCNT xx@ drop hex. cr
  eint
  $10 0 do i hex. 
    ."  PMM = " PMM i 0 d+ x@ hex.
    ."  RTC = " RTC i 0 d+ x@ hex.
  cr
  2 +loop
;